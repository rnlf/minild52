#include "log.hpp"
#include "draw.hpp"
#include "laser.hpp"
#include "level.hpp"
#include "loadsprite.hpp"
#include "loadsound.hpp"

#include "mixer.hpp"

extern Mixer mixer;

#include <Box2D/Box2D.h>

Laser::Laser(Level& level, int x, int y) 
  : Entity(level, 0) 
  , blocker(x / 16, y / 16) {

  sprite = loadSprite("data/sprite/laser.spr");
  laserOnSound = loadSound("data/sound/laseron.wav");
  laserOffSound = loadSound("data/sound/laseroff.wav");

  b2BodyDef bodyDef;
  bodyDef.type = b2_staticBody;
  bodyDef.position.Set(float(x) / 16.0, float(y) / 16.0);
  body = level.world.CreateBody(&bodyDef);

  b2PolygonShape box;
  box.SetAsBox(7.5 / 16.0, 7.5 / 16.0, b2Vec2(0.5, 0.5), 0);
  b2FixtureDef fixtureDef;
  fixtureDef.shape = &box;
  auto f = body->CreateFixture(&fixtureDef);
  blocker.fixture = f;

  level.addLaserInfluencer(&blocker);

}


const Laser::Dir Laser::dirs[8] = {
  {-1, 0, "0_on", "0_off"},
  {-1, 1, "", ""},
  { 0, 1, "2_on", "2_off"},
  { 1, 1, "", ""},
  { 1, 0, "4_on", "4_off"},
  { 1, -1, "", ""},
  { 0, -1, "6_on", "6_off"},
  { -1, -1, "", ""}
};




void Laser::traceBeam(int sx, int sy, int dir) {
  int dx = dirs[dir].x;
  int dy = dirs[dir].y;
  int ex = sx + dx;
  int ey = sy + dy;
  while(!level.blocksLaser(ex, ey)) {

    auto lis = level.getLaserInfluencersAt(ex, ey);
    for(auto li: lis) {
      if(li == &blocker)
        continue;
      segments.emplace_back(sx * 16 + 8, sy * 16 + 8, ex * 16 + 8, ey * 16 + 8);
      sx = ex;
      sy = ey;
      int olddir = dir;
      li->influence(dir, sx, sy);
      if(dir < 0) {
        auto & s = segments.back();
        s.x2 -= dirs[olddir].x * (dir + 16);
        s.y2 -= dirs[olddir].y * (dir + 16);
        return;
      }

      dx = dirs[dir].x;
      dy = dirs[dir].y;
    }
    ex += dx;
    ey += dy;
  }

  if(dir >= 0)
    segments.emplace_back(sx * 16 + 8, sy * 16 + 8, ex * 16 + 8, ey * 16 + 8);
}


void Laser::draw() const {
  auto const& pos = body->GetPosition();
  sprite.draw(pos.x * 16, pos.y * 16);
  for(auto const& l: segments)
    line(l.x1, l.y1, l.x2, l.y2, 0x00FF0000, replace);
}

void Laser::setState(bool value) {
  if(state != value) {
    state = value;
    if(state) {
      mixer.play(laserOnSound);
      sprite.play(dirs[dir].on);
    } else {
      mixer.play(laserOffSound);
      sprite.play(dirs[dir].off);
    }
  }
}


void Laser::setProperty(std::string property, std::string value) {
  if(property == "state")
    setState(value == "on");
  if(property == "dir")
    setDir(atoi(value.c_str()));
}


void Laser::setDir(int dir) {
  this->dir = dir;
  if(state)
    sprite.play(dirs[dir].on);
  else
    sprite.play(dirs[dir].off);
}

void Laser::update(double dt) {
  segments.clear();
  auto const& pos = body->GetPosition();
  if(state) {
    traceBeam(pos.x , pos.y, dir);
  }

}
