#include "levelcontactlistener.hpp"
#include "entitycontactlistener.hpp"

static inline void notifyIfNotNull(
    void* listener,
    void (EntityContactListener::*f)(b2Fixture*),
    b2Fixture* fixture) {

  if(listener) {
    auto ecl = reinterpret_cast<EntityContactListener*>(listener);
    (ecl->*f)(fixture);
  }
}

static inline void notifyAllNonNull(b2Contact* contact, 
    void (EntityContactListener::*f)(b2Fixture*)) {
  
  notifyIfNotNull(contact->GetFixtureA()->GetUserData(), f, contact->GetFixtureB());
  notifyIfNotNull(contact->GetFixtureB()->GetUserData(), f, contact->GetFixtureA());
  notifyIfNotNull(contact->GetFixtureA()->GetBody()->GetUserData(), f, contact->GetFixtureB());
  notifyIfNotNull(contact->GetFixtureB()->GetBody()->GetUserData(), f, contact->GetFixtureA());
}

void LevelContactListener::BeginContact(b2Contact* contact) {
  notifyAllNonNull(contact, &EntityContactListener::beginContact);
}

void LevelContactListener::EndContact(b2Contact* contact) {
  notifyAllNonNull(contact, &EntityContactListener::endContact);
}

