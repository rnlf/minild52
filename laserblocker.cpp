#include "laserblocker.hpp"
#include "laser.hpp"

LaserBlocker::LaserBlocker(int x, int y, b2Fixture * fixture)
  : LaserInfluencer(x, y)
  , fixture(fixture)
  /*
  , px(x * 16)
  , py(y * 16) */
  {}

void LaserBlocker::influence(int& indir, int qx, int qy) {
  b2AABB aabb;
  auto const& trans = fixture->GetBody()->GetTransform();
  fixture->GetShape()->ComputeAABB(&aabb, trans, 0);
  b2RayCastOutput out;
  b2RayCastInput input = {
    {(qx - Laser::dirs[indir].x) + 0.5, (qy - Laser::dirs[indir].y) + 0.5},
    {qx + 0.5, qy + 0.5},
    1};

  if(aabb.RayCast(&out, input)) {
    indir = -16 * out.fraction;
    if(indir >= 0)
      indir = -1;

  }
}

bool LaserBlocker::at(int qx, int qy) const {
  auto const& aabb = fixture->GetAABB(0);
  return qx >= floor(aabb.lowerBound.x-1) &&
         qx <= ceil(aabb.upperBound.x+2) &&
         qy >= floor(aabb.lowerBound.y-1) &&
         qy <= ceil(aabb.lowerBound.y+1);
}

/*
void LaserBlocker::setPixelpos(int nx, int ny) {
  px = nx; py = ny;
  x = (int(px) + 8) / 16;
  y = (int(py) + 8) / 16;
}
*/
