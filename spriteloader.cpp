#include <iostream>
#include <stdexcept>
#include <fstream>
#include <sstream>
#include <string>
#include <regex>
#include <vector>
#include <iterator>
#include "loadimage.hpp"
#include "spriteloader.hpp"

class SpriteLoaderException: public std::runtime_error {
public:
  SpriteLoaderException()
    : std::runtime_error("Invalid file format while loading sprite") {}
};

BaseSprite loadSPR(std::string filename) {
  std::ifstream input(filename);
  
  BaseSprite sprite;

  std::string line;
  while(input) {
    std::getline(input, line);
    std::istringstream linestr(line);
    auto tokens = std::vector<std::string>(
      std::istream_iterator<std::string>(linestr),
      std::istream_iterator<std::string>()
    );

    if(tokens.size() < 1 || tokens[0][0] == '#')
      continue;

    if(tokens[0] == "image") {
      if(tokens.size() < 4 || tokens.size() > 5)
        throw SpriteLoaderException();

      BlitMode mode = BlitMode::REPLACE;

      if(tokens.size() == 5) {
        if(tokens[4] == "replace")
          mode = BlitMode::REPLACE;
        else if(tokens[4] == "alphatest")
          mode = BlitMode::ALPHATEST;
        else if(tokens[4] == "alphablend")
          mode = BlitMode::ALPHABLEND;
        else
          throw SpriteLoaderException();
      }
     

      sprite = BaseSprite(loadImage("data/image/" + tokens[1]), atoi(tokens[2].c_str()), atoi(tokens[3].c_str()), mode );

    } else if(tokens[0] == "anim") {
      if(tokens.size() < 4)
        throw SpriteLoaderException();

      std::vector<unsigned> frames;
      frames.reserve(tokens.size() - 2);
      std::transform(tokens.begin() + 3, tokens.end(), std::back_inserter(frames),
          [](auto const& f) { return atoi(f.c_str()); });
      sprite.addAnimation(tokens[1], atof(tokens[2].c_str()), frames);

    } else {
      throw SpriteLoaderException();
    }
  }

  return sprite;
}
