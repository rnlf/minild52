#include <algorithm>
#include <iterator>
#include <iostream>
#include "level.hpp"
#include <tmxparser.h>
#include "loadimage.hpp"
#include "entityfactory.hpp"

class TMXFileLoadException: public std::runtime_error {
public:
  TMXFileLoadException()
    : std::runtime_error("Could not load level") {}
};

Level loadLevel(std::string filename) {
  tmxparser::TmxMap map;
  tmxparser::TmxReturn error = tmxparser::parseFromFile(filename, &map);
  if(error != tmxparser::kSuccess)
    throw TMXFileLoadException();

  std::string const& file = map.tilesetCollection[0].image.source;
  std::string::size_type p = file.rfind('/');

  std::string tilesetname = "data/image/" + ((p == std::string::npos) ? file : file.substr(p + 1));

  std::shared_ptr<Surface> tileset = loadImage(tilesetname);

  std::vector<unsigned> tiles;
  std::vector<unsigned> background;
  std::vector<unsigned> front;
  tiles.reserve(map.width * map.height);
  auto centerLayer = std::find_if(map.layerCollection.begin(), map.layerCollection.end(), [](auto const& layer){return layer.name == "center";});
  std::transform(
    centerLayer->tiles.begin(),
    centerLayer->tiles.end(),
    std::back_inserter(tiles),
    [](auto const& tile) { return tile.gid; });

  auto backgroundLayer = std::find_if(map.layerCollection.begin(), map.layerCollection.end(), [](auto const& layer){return layer.name == "back";});
  std::transform(
    backgroundLayer->tiles.begin(),
    backgroundLayer->tiles.end(),
    std::back_inserter(background),
    [](auto const& tile) { return tile.gid; });

  auto frontLayer = std::find_if(map.layerCollection.begin(), map.layerCollection.end(), [](auto const& layer){return layer.name == "front";});
  std::transform(
    frontLayer->tiles.begin(),
    frontLayer->tiles.end(),
    std::back_inserter(front),
    [](auto const& tile) { return tile.gid; });

  Level level(map.width, map.height);

  for(auto const& t: map.tilesetCollection[0]._tiles) {
    auto it = t.propertyMap.find("nolaser");
    if(it != t.propertyMap.end() && it->second == "true") {
      level.addNoLaser(t.id);
    }
  }


  level.setTileset(tileset);
  level.setCenterLayer(std::move(tiles));
  level.setBackgroundLayer(std::move(background));
  level.setFrontLayer(std::move(front));


  auto entityLayer = std::find_if(map.objectGroupCollection.begin(), map.objectGroupCollection.end(), [](auto const& layer){return layer.name == "entities";});

  for(auto const& ent: entityLayer->objects) {
    createEntity(level, ent);
  }

  level.levelname = filename;

  return level;
}
