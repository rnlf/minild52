#include "entityfactory.hpp"

#include "log.hpp"
#include "level.hpp"
#include <tmxparser.h>
#include <map>
#include <string>
#include "exit.hpp"
#include "jojo.hpp"
#include "laser.hpp"
#include "switch.hpp"
#include "mirror.hpp"
#include "pulsecounter.hpp"
#include "oscillator.hpp"
#include "sensor.hpp"
#include "glass.hpp"
#include "dynblock.hpp"
#include "gate.hpp"
#include "logicgate.hpp"

template<typename E>
std::unique_ptr<Entity> create(Level& level, tmxparser::TmxObject const& objdef) {
  std::unique_ptr<Entity> e = std::make_unique<E>(level, objdef.x, objdef.y);
  return e;
}

std::unique_ptr<Gate> createGate(Level& level, tmxparser::TmxObject const& objdef) {
  auto e = std::make_unique<Gate>(level, objdef.x, objdef.y);
  e->x2 = e->x1 + objdef.width;
  e->y2 = e->y1 + objdef.height;
  e->makeBody();
  return e;
}


static std::map<std::string, std::function<std::unique_ptr<Entity>(Level&, tmxparser::TmxObject const&)>> entityFactoryFunctions = {
  {"exit", create<Exit>},
  {"jojo", create<Jojo>},
  {"laser", create<Laser>},
  {"switch", create<Switch>},
  {"mirror", create<Mirror>},
  {"pulsecounter", create<PulseCounter>},
  {"oscillator", create<Oscillator>},
  {"sensor", create<Sensor>},
  {"glass", create<Glass>},
  {"dynblock", create<DynBlock>},
  {"gate", createGate},
  {"logic", create<LogicGate>}
};

void createEntity(Level& level, tmxparser::TmxObject const& objdef) {
  auto f = entityFactoryFunctions.find(objdef.type);
  if(f != entityFactoryFunctions.end()) {
    auto entity = (f->second)(level, objdef);

    for(auto const& prop: objdef.propertyMap) {
      entity->setProperty(prop.first, prop.second);
    }

    level.addEntity(std::move(entity), objdef.name);
  }
}
