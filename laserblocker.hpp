#pragma once

#include "laserinfluencer.hpp"
#include <Box2D/Box2D.h>

class b2Fixture;

class LaserBlocker: public LaserInfluencer {
//  int px, py;
public:
  b2Fixture *fixture;
  LaserBlocker(int x, int y, b2Fixture *fixture = nullptr);

  void influence(int& indir, int qx, int qy) override;

  bool at(int qx, int qy) const override;
  //void setPixelpos(int nx, int ny);
};
