#pragma once

#include "basesprite.hpp"

struct vec2;

class Sprite {
  float frame;
  std::shared_ptr<BaseSprite> baseSprite;
  SpriteAnimation *currentAnimation;

  std::function<void(Sprite&)> onFinishAnim;
  std::function<void(Sprite&)> nextFinishAnim;
  bool haveNewFinish = false;

public:
  Sprite();
  Sprite(std::shared_ptr<BaseSprite> base, std::string anim="");

  void play(std::string name, std::function<void(Sprite&)> finishHandler=nullptr, float frame = 0.0f);
  void update(float dt);
  void draw(int x, int y, bool flipx = false, bool flipy = false) const;

  bool valid() const;
  float getFrame() const;

  BaseSprite const& getBase() const;
  std::string getAnimation() const;

};
