#include "dynblock.hpp"
#include "loadsprite.hpp"
#include "level.hpp"
#include "sound.hpp"
#include <memory>
#include "mixer.hpp"
#include "loadsound.hpp"

extern Mixer mixer;

DynBlock::DynBlock(Level& level, int x, int y) 
  : Entity(level) 
  , blocker((x + 8) / 16, (y + 8) / 16) {

  b2BodyDef bodyDef;
  bodyDef.type = b2_dynamicBody;
  bodyDef.position.Set(float(x) / 16.0, float(y) / 16.0);
  bodyDef.fixedRotation = true;
  bodyDef.linearDamping = 2.0f;
  bodyDef.gravityScale = 1.0f;
  bodyDef.allowSleep = false;
  body = level.world.CreateBody(&bodyDef);

  b2PolygonShape box;
  box.SetAsBox(0.5f, 0.49f, b2Vec2(0.5 - 1 / 16.0, 0.5 - 0.5f / 16.0f), 0);
  b2FixtureDef fixtureDef;
  fixtureDef.density = 3.0f;
  fixtureDef.friction = 0.1;
  fixtureDef.shape = &box;
  fixture = body->CreateFixture(&fixtureDef);

  sprite = loadSprite("data/sprite/dynblock.spr");
  sprite.play("exist");
  blocker.fixture = fixture;

  level.addLaserInfluencer(&blocker);

  thudSound = loadSound("data/sound/thud.wav");
}

void DynBlock::draw() const {
  auto pos = body->GetPosition();
  sprite.draw(pos.x * 16,pos.y * 16);
}

void DynBlock::update(double dt) {
  auto pos = body->GetPosition();

  float vspeed = body->GetLinearVelocity().y;

  if(vspeed < 5.0f && oldVSpeed >= 5.0f)
    mixer.play(thudSound);


  oldVSpeed = vspeed;
  //blocker.setPixelpos(pos.x * 16, pos.y * 16);
}
