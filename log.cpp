#include "log.hpp"
#include <cstdio>
#include <cstdarg>



void log(char const* fmt, ...) {
  FILE* logfile = fopen("log.txt", "a");
  
  va_list va;
  va_start(va, fmt);
  vfprintf(logfile, fmt, va);
  va_end(va);

  fflush(logfile);
  fclose(logfile);
}
