#pragma once

#include "entitycontactlistener.hpp"

class GenericContactListener: public EntityContactListener {
public:
  std::function<void(b2Fixture*)> beginContactFunc;
  std::function<void(b2Fixture*)> endContactFunc;

  virtual void beginContact(b2Fixture *fixture) override;
  virtual void endContact(b2Fixture *fixture) override;

  void *data = nullptr;
};
