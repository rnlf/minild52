#include "waveloader.hpp"
#include "resourcemanager.hpp"


static ResourceManager<Sound, Sound (*)(std::string)> soundManager(loadWaveFile);

std::shared_ptr<Sound> loadSound(std::string name) {
  return soundManager.load(name);
}

