#include "log.hpp"
#include <sstream>
#include "level.hpp"
#include "oscillator.hpp"

Oscillator::Oscillator(Level& level, int, int) 
  : Entity(level) {
}

void Oscillator::setProperty(std::string property, std::string value) {
  if(property == "state") {
    setState(value == "on");
  } else if(property == "values") {
    values.clear();
    std::string::size_type i = 0;
    for(;;) {
      std::string::size_type j = value.find(',', i);
      values.push_back(value.substr(i, j-i));
      if(j == std::string::npos)
        break;
      i = j+1;
    }
  } else if(property == "pulse") {
    setPulse(atoi(value.c_str()));
  } else if(property == "target") {
    target = value;
  } else if(property == "frequency") {
    setFrequency(atof(value.c_str()));
  }
}

void Oscillator::setFrequency(float freq) {
  frequency = freq;
}

void Oscillator::postTarget() {
  setPulse(pulse+1);

  if(state)
    timer.delay(0.5f / frequency, [=](){ postTarget(); });
}

void Oscillator::startTimer() {
  timer.delay(0.5f / frequency, [=](){ postTarget(); });
}

void Oscillator::setState(bool value) {
  if(!state)
    startTimer();

  state = value;
}

void Oscillator::setValues(std::vector<std::string> values) {
  this->values = values;
}

void Oscillator::setPulse(int i) {
  pulse = i % values.size();
  sendProperty(target, values[pulse]);
}

void Oscillator::update(double dt) {
  timer.update(dt);
}
