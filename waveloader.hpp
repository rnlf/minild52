#pragma once

#include <string>
#include "sound.hpp"

Sound loadWaveFile(std::string filename);
