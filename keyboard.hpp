#pragma once

#include <go32.h>

void keyboardInit();
void keyboardRelease();
void keyboardPump();
bool keyboardPressed(int scancode);
bool keyboardReleased(int scancode);
bool keyboardDown(int scancode);
