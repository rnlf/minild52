#include "loadsprite.hpp"
#include "resourcemanager.hpp"
#include "spriteloader.hpp"

static ResourceManager<BaseSprite, BaseSprite (*)(std::string)> spriteManager(loadSPR);

std::shared_ptr<BaseSprite> loadBaseSprite(std::string name) {
  return spriteManager.load(name);
}

Sprite loadSprite(std::string name, std::string anim) {
  return Sprite(loadBaseSprite(name), anim);
}
