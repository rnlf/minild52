#pragma once

#include "entitycontactlistener.hpp"

class ContactCounter: public EntityContactListener {
  int count = 0;
public:
  void beginContact(b2Fixture *fixture) override;
  void endContact(b2Fixture *fixture) override;
  int getCount() const noexcept;
};
