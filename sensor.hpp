#pragma once

#include "entity.hpp"

#include "sensorinfluencer.hpp"

class b2Body;
class b2Fixture;

struct Sensor: Entity {
  b2Body *body;

  SensorInfluencer influencer;
  bool hitLastFrame = false;
  bool hitThisFrame = false;
  bool wasOn = false;
  std::string target;

  Sprite sprite;
  Sensor(Level& level, int x, int y);

  void setProperty(std::string property, std::string value) override;
  void setTarget(std::string target);
  void draw() const override;
  void update(double dt) override;
};
