#include "surface.hpp"

Surface::Surface()
  : width(0)
  , height(0) {}

Surface::Surface(unsigned width, unsigned height) 
  : storage(std::make_unique<uint32_t[]>(width*height))
  , width(width)
  , height(height) { }


unsigned Surface::getWidth() const noexcept {
  return width;
}


unsigned Surface::getHeight() const noexcept {
  return height;
}


uint32_t* Surface::getBuffer() const noexcept {
  return storage.get();
}

