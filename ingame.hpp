#pragma once

#include "gamestate.hpp"
#include "level.hpp"

class StateManager;

class Ingame: public Gamestate {
  Level level;
public:
  Ingame(StateManager& manager, std::string levelname);

  void update(double dt) override;
  void draw() const override;


};
