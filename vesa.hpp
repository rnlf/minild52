#pragma once

#include "dosmemory.hpp"
#include <memory>
#include "surface.hpp"

class Vesa {
  struct VbeInfoBlockData {
     uint32_t         vbeSignature;
     uint8_t          vbeMinorVersion;
     uint8_t          vbeMajorVersion;
     FarPtr<char>     oemStringPtr;
     uint8_t          capabilities[4];
     FarPtr<uint16_t> videoModePtr;
     uint16_t         totalMemory;
  };

  static_assert(sizeof(VbeInfoBlockData) == 20,
                "Incorrect alignment for VbeInfoBlockData");

  int originalMode;
  UniqueFarPtr<VbeInfoBlockData> infoBlock;
  MappedPhysicalMemory<uint32_t> vram;
  //std::unique_ptr<uint32_t[]> offscreenVram;
  Surface surface;
  uint32_t winFuncPtr;
  size_t scanlineSize;

  void* setWindowCallAddr;
  void* setDisplayStartAddr;
  void* setPrimaryPaletteAddr;

  std::unique_ptr<uint8_t[]> protectedModeCopy;

public:
  Vesa();
  ~Vesa();
  void setMode(int mode, bool clear=true, bool windowed=false);
  int getMode();
  size_t    getScanlineSize() const noexcept;
  Surface&  getSurface() noexcept;
  Surface*  getSurfacePtr() noexcept;

  void setPage(unsigned page) const noexcept;
  void reset();

};
