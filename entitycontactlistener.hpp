#pragma once

#include <functional>

class b2Fixture;

class EntityContactListener {
public:
  int type = 0;

  virtual void beginContact(b2Fixture *fixture) = 0;
  virtual void endContact(b2Fixture *fixture) = 0;
};
