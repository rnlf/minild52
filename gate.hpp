#pragma once

#include "entity.hpp"
#include "flux.hpp"
#include "laserblocker.hpp"

class Level;

class b2Body;
class b2Fixture;

struct Gate: public Entity {
  std::shared_ptr<Surface> surface;
  enum class Direction {
    UP, DOWN, LEFT, RIGHT
  };

  flux::group tween;

  b2Body *body;
  b2Fixture *fixture;

  Direction direction = Direction::LEFT;

  int x1, y1;
  int x2, y2;
  float open = 0.0f;

  LaserBlocker blocker;

  Gate(Level& level, int x, int y);

  void draw() const override;

  void setProperty(std::string property, std::string value);
  void setDirection(Direction dir);
  void setState(bool state);
  void update(double dt) override;

  void makeBody();
  void updatePhysics();

  float time = 4.0f;

  void setTime(float time);
};
