#include "contactcounter.hpp"

#include <Box2D/Box2D.h>

void ContactCounter::beginContact(b2Fixture *fixture) {
  if(!fixture->IsSensor())
    ++count;
}

void ContactCounter::endContact(b2Fixture *fixture) {
  if(!fixture->IsSensor())
    --count;
}

int ContactCounter::getCount() const noexcept {
  return count;
}

