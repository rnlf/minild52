#pragma once

#include <tmxparser.h>
#include "entity.hpp"

void createEntity(Level& level, tmxparser::TmxObject const& objdef);
