#pragma once

#include "laserinfluencer.hpp"

class Sensor;

class SensorInfluencer: public LaserInfluencer {
  Sensor& sensor;
  bool wasOn = false;
public:
  SensorInfluencer(Sensor& sensor, int x, int y);

  void influence(int& dir, int, int) override;

};
