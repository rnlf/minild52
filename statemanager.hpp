#pragma once

#include <memory>
#include "gamestate.hpp"

class StateManager {
  std::shared_ptr<Gamestate> currentState;
  std::shared_ptr<Gamestate> nextState;

public:
  StateManager();
  void process(double dt);
  void switchState(std::shared_ptr<Gamestate> next);
};
