#pragma once

#include <vector>
#include <memory>
#include "surface.hpp"
#include <Box2D/Box2D.h>
#include "levelcontactlistener.hpp"
#include "entity.hpp"
#include <unordered_map>

#include "laserinfluencer.hpp"

class Ingame;

class Level {
  std::shared_ptr<Surface> tileset;
  Surface backgroundImage;
  unsigned width;
  unsigned height;
  unsigned tilesetRow;
  std::vector<unsigned> tiles;
  std::vector<unsigned> background;
  LevelContactListener contactListener;
  void buildStaticCollisionMesh();
  void addStaticLine(float x1, float y1, float x2, float y2);

  std::vector<std::unique_ptr<Entity>> entities;
  std::unordered_map<std::string, Entity*> namedEntities;

  std::vector<LaserInfluencer*> laserInfluencers;

  std::vector<Entity*> layers[3];

  struct DrawTile {
    int dstx;
    int dsty;
    int srcx;
    int srcy;
    int srcw;
    int srch;
  };

  std::vector<DrawTile> centerTiles;
  std::vector<DrawTile> frontTiles;

  std::vector<int> noLaser;

public:

  void addEntity(std::unique_ptr<Entity>&& entity, std::string name="");
  int getTile(int col, int row) const noexcept;
  b2World  world;
  Level(unsigned width, unsigned height);
  void draw() const;
  void collideEntity(Entity& entity) const;
  void update(double dt);
  void setTileset(std::shared_ptr<Surface> tileset);
  void setCenterLayer(std::vector<unsigned>&& tiles);
  void setBackgroundLayer(std::vector<unsigned>&& tiles);
  void setFrontLayer(std::vector<unsigned>&& tiles);

  Entity* getNamedEntity(std::string name) const;

  void addLaserInfluencer(LaserInfluencer* influencer);
  void removeLaserInfluencer(LaserInfluencer* influencer);
  std::vector<LaserInfluencer*> getLaserInfluencersAt(int x, int y) const;

  void addNoLaser(int idx);
  bool blocksLaser(int col, int row) const noexcept;

  Ingame* gamestate;
  std::string levelname;
};
