#include "draw.hpp"
#include "sprite.hpp"
#include "video.hpp"
#include "vector.hpp"

Sprite::Sprite() 
  : frame(0.0f) { }

Sprite::Sprite(std::shared_ptr<BaseSprite> base, std::string anim)
  : Sprite() {

  baseSprite = base;
  currentAnimation = base->getAnimation(anim);
}

void Sprite::play(std::string name, std::function<void(Sprite&)> finishHandler, float frame) {
  nextFinishAnim = finishHandler;
  haveNewFinish = true;
  auto newanim = baseSprite->getAnimation(name);
  if(newanim != currentAnimation) {
    this->frame = frame;
    currentAnimation = newanim;
  }
}

void Sprite::update(float dt) {
  if(haveNewFinish)
    onFinishAnim = nextFinishAnim;

  frame = frame + dt * currentAnimation->fps;
  if(frame >= currentAnimation->frames.size() - 1) {
    frame = fmodf(frame, currentAnimation->frames.size());
    if(onFinishAnim)
      onFinishAnim(*this);
  }
}

void Sprite::draw(int x, int y, bool flipx, bool flipy) const {
  auto const& f = currentAnimation->frames[int(frame)];
  blit(baseSprite->getSurface(), x, y, 
       f.x, f.y, f.w, f.h,
       baseSprite->getBlitMode(), flipx, flipy);
}

bool Sprite::valid() const {
  return bool(baseSprite);
}

BaseSprite const& Sprite::getBase() const {
  return *baseSprite;
}

float Sprite::getFrame() const {
  return frame;
}

std::string Sprite::getAnimation() const {
  return currentAnimation->name;
}
