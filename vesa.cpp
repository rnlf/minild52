#include <iomanip>
#include <iostream>
#include <cstdint>
#include <stdexcept>
#include <cstring>
#include <memory>
#include <dpmi.h>
#include <sys/nearptr.h>
#include <sys/farptr.h>
#include "dosmemory.hpp"

#include "vesa.hpp"

const uint32_t VESA_SIGNATURE = 0x41534556;
const uint16_t CLEAR_FLAG     = 0x4000;
const uint16_t WINDOWED_FLAG  = 0x8000;

const uint16_t MODE_MASK      = 0x3FFF;
const uint16_t VBE_CONTROLLER_INFO_CMD = 0x4f00;
const uint16_t VBE_MODE_INFO_CMD       = 0x4f01;
const uint16_t VBE_SET_MODE_CMD        = 0x4f02;
const uint16_t VBE_GET_MODE_CMD        = 0x4f03;
const uint16_t VBE_RETURN_PROTECTED_MODE_INTERFACE = 0x4f0a;

const uint16_t BIOS_INTERRUPT  = 0x10;

struct ModeInfoBlock {
  uint16_t modeAttributes;
  uint8_t  winAAttributes;
  uint8_t  winBAttributes;
  uint16_t winGranularity;
  uint16_t winSize;
  uint16_t winASegment;
  uint16_t winBSegment;
  uint32_t winFuncPtr;
  uint16_t bytesPerScanline;
  uint16_t xResolution;
  uint16_t yResolution;
  uint8_t  xCharSize;
  uint8_t  yCharSize;
  uint8_t  numberOfPlanes;
  uint8_t  bitsPerPixel;
  uint8_t  numberOfBanks;
  uint8_t  memoryModel;
  uint8_t  bankSize;
  uint8_t  numberOfImagePages;
  uint8_t  reserved__;
  uint8_t  redMaskSize;
  uint8_t  redFieldPosition;
  uint8_t  greenMaskSize;
  uint8_t  greenFieldPosition;
  uint8_t  blueMaskSize;
  uint8_t  blueFieldPosition;
  uint8_t  reservedMaskSize;
  uint8_t  reservedFieldPosition;
  uint8_t  directColorModeInfo;
  uint32_t physBasePtr;
  uint32_t offscreenMemOffset;
  uint16_t offscreenMemSize;
};

static_assert(sizeof(ModeInfoBlock) == 52,
              "Incorrect alignment for ModeInfoBlock");


class VBENotAvailable: public std::runtime_error {
public:
  VBENotAvailable()
    : std::runtime_error("VESA BIOS Extension >= 2.0 not available") {}
};


class VBEBadWindowAttributes: public std::runtime_error {
public:
  VBEBadWindowAttributes()
    : std::runtime_error("VESA mode window attributes insufficient") {}
};


Vesa::Vesa()
  : infoBlock(makeUniqueFar<VbeInfoBlockData>()) {

  // Get controller info
  __dpmi_regs regs;
  regs.x.ax = VBE_CONTROLLER_INFO_CMD;
  regs.x.es = infoBlock.get().getSegment();
  regs.x.di = 0;
  __dpmi_int(BIOS_INTERRUPT, &regs);

  if(infoBlock->vbeSignature != VESA_SIGNATURE) {
    throw VBENotAvailable();
  }

  std::cout << "VESA BIOS Extension Version "
    << int(infoBlock->vbeMajorVersion) << "."
    << int(infoBlock->vbeMinorVersion) << std::endl;

  if(infoBlock->vbeMajorVersion < 2) {
    throw VBENotAvailable();
  }

  std::cout << "Video Adapter: " << infoBlock->oemStringPtr.get() << ", "
    << (infoBlock->totalMemory*64) << "KB VRAM"
    << std::endl;

  originalMode = getMode();

  regs.x.ax = VBE_RETURN_PROTECTED_MODE_INTERFACE;
  regs.x.bx = 0;
  __dpmi_int(BIOS_INTERRUPT, &regs);

  auto tableFarPtr = FarPtr<uint8_t const>(regs.x.es, regs.x.di);

  protectedModeCopy = std::unique_ptr<uint8_t[]>(new uint8_t[regs.x.cx]);

  memcpy(protectedModeCopy.get(), tableFarPtr.get(), regs.x.cx);

  setDisplayStartAddr = protectedModeCopy.get()
    + *reinterpret_cast<uint16_t*>(protectedModeCopy.get()+2);
}


Vesa::~Vesa() {
  reset();
}


void Vesa::reset() {
  __dpmi_regs regs;
  regs.x.ax = VBE_SET_MODE_CMD;
  regs.x.bx = originalMode;
  __dpmi_int(BIOS_INTERRUPT, &regs);
}


void Vesa::setMode(int mode, bool clear, bool windowed) {
  __dpmi_regs regs;

  // Get mode info
  auto modeInfo = makeUniqueFar<ModeInfoBlock>();
  regs.x.ax = VBE_MODE_INFO_CMD;
  regs.x.cx = mode & MODE_MASK;
  regs.x.es = modeInfo.get().getSegment();
  regs.x.di = modeInfo.get().getOffset();
  __dpmi_int(BIOS_INTERRUPT, &regs);

  // Store VRAM pointer
  vram = MappedPhysicalMemory<uint32_t>(
    modeInfo->physBasePtr, infoBlock->totalMemory << 16);

  if(modeInfo->winAAttributes != 7)
    throw VBEBadWindowAttributes();

  // Set mode
  regs.x.ax = VBE_SET_MODE_CMD;
  regs.x.bx = (mode & MODE_MASK)
            | (clear    ? CLEAR_FLAG    : 0)
            | (windowed ? WINDOWED_FLAG : 0);
  __dpmi_int(BIOS_INTERRUPT, &regs);

  surface = Surface(320, 200); // TODO use real resolution of mode
}


int Vesa::getMode() {
  __dpmi_regs regs;
  regs.x.ax = VBE_GET_MODE_CMD;
  __dpmi_int(BIOS_INTERRUPT, &regs);

  return regs.x.bx;
}


Surface& Vesa::getSurface() noexcept {
  return surface;
}


Surface* Vesa::getSurfacePtr() noexcept {
  return &surface;
}


size_t Vesa::getScanlineSize() const noexcept {
  return scanlineSize;
}


void Vesa::setPage(unsigned page) const noexcept {
  memcpy(vram.get(), surface.getBuffer(), 320*200*4);
  


  return;
  uint32_t base = reinterpret_cast<uint32_t>(vram.get());
  uint32_t addr = base + 320*200*page;
  uint16_t low =  addr & 0xFFFF;
  uint16_t high = (addr >> 16) & 0xFFFF;
  asm ("pusha\n" \
       "movw $0x4F07, %%ax\n" \
       "movw $0x80, %%bx\n" \
       "movw %1, %%cx\n" \
       "movw %2, %%dx\n" \
       "call *%0\n" \
       "popa\n" \
    :
    : "r" (setDisplayStartAddr), "r" (low), "r" (high)
    : "%ax", "%bx", "%cx", "%dx");
}
