#include "log.hpp"
#include "keyboard.hpp"
#include "jojo.hpp"
#include "loadsprite.hpp"
#include <Box2D/Box2D.h>

#include "level.hpp"
#include "loadsound.hpp"
#include "mixer.hpp"
#include "leveldonestate.hpp"
#include "statemanager.hpp"
#include "ingame.hpp"
#include "mixer.hpp"

extern Mixer mixer;

Jojo::Jojo(Level& level, int x, int y)
  : Entity(level, 2) 
  , influencer(this, x, y) {
  sprite = loadSprite("data/sprite/jojo.spr");
  jumpSound = loadSound("data/sound/jump.wav");
  deadSound = loadSound("data/sound/dead.wav");

  jojoContact.type = Jojo::TYPE;
  jojoContact.data = this;

  b2BodyDef bodyDef;
  bodyDef.type = b2_dynamicBody;
  bodyDef.position.Set(float(x) / 16.0, float(y) / 16.0);
  bodyDef.fixedRotation = true;
  bodyDef.linearDamping = 1.0f;
  bodyDef.allowSleep = false;
  body = level.world.CreateBody(&bodyDef);
  b2PolygonShape dynamicBox;
  // TODO make shape a bit angled on the top so it is easier to jump into
  //      openings
/*
  b2Vec2 shape[] = {
    { 0.4f, 0.0f },
    { 0.0f, 0.2f },
    { 0.0f, 1.0f },
    { 0.3f, 1.0f },
    { 0.7f, 1.0f },
    { 0.8f, 1.0f },
    { 0.8f, 0.2f },
    { 0.6f, 0.0f }
  };

  for(int i = 0; i < 8; i++) {
    shape[i].x -= 1.0f / 16.0f;
    shape[i].y -= 1.0f / 16.0f;
  } */
  dynamicBox.SetAsBox(0.3f, 0.5f, b2Vec2(0.5-1.0/16.0,0.5-1.0/16.0), 0);
  //dynamicBox.Set(shape, 8);
  b2FixtureDef fixtureDef;
  fixtureDef.shape = &dynamicBox;
  fixtureDef.density = 2.0f;
  fixtureDef.friction = .1f;
  fixtureDef.restitution = 0.2f;
  body->CreateFixture(&fixtureDef)->SetUserData(&jojoContact);

  fixtureDef.friction = 0.0f;
  dynamicBox.SetAsBox(0.05f, 0.49f, b2Vec2(0.15-1.0/16.0,0.5-1.0/16.0), 0);
  body->CreateFixture(&fixtureDef);
  dynamicBox.SetAsBox(0.05f, 0.49f, b2Vec2(0.85-1.0/16.0,0.5-1.0/16.0), 0);
  body->CreateFixture(&fixtureDef);

  fixtureDef.isSensor = true;

  dynamicBox.SetAsBox(0.3f, 0.1f, b2Vec2(0.5f-1.0/16.0,0.95f), 0);
  foot = body->CreateFixture(&fixtureDef);
  foot->SetUserData(&footCounter);

  level.addLaserInfluencer(&influencer);

}

void Jojo::update(double dt) {
  if(state == State::ALIVE) {

    if(keyboardDown(0x11) || keyboardDown(0x148))
      jump();

    if(keyboardDown(0x1E) || keyboardDown(0x14B))
      run(-1);

    if(keyboardDown(0x20) || keyboardDown(0x14D))
      run(1);

    if(footCounter.getCount() == 0) {
      sprite.play("jump");
    } else {
      if(fabs(body->GetLinearVelocity().x) > 0.01)
        sprite.play("walk");
      else
        sprite.play("idle");
    }

    if(body->GetPosition().y > 14) {
      kill();
    }
  }
  sprite.update(dt);
  timer.update(dt);

  auto const& pos = body->GetPosition();

  influencer.x = pos.x + 8.0 / 16.0;
  influencer.y = pos.y + 5.0 / 16.0;
}
    
void Jojo::kill() {
  mixer.play(deadSound);
  state = State::DYING;
  sprite.play("dying", [=](auto &spr) {
    spr.play("dead");
    log("dead\n");
    auto a = std::make_shared<LevelDoneState>(level.gamestate->manager, level.levelname, "dethscrn");
    level.gamestate->manager.switchState(a);
  });
}

void Jojo::jump() {
  if(footCounter.getCount() > 0 && canjump) {
    canjump = false;
    body->SetLinearVelocity(b2Vec2(body->GetLinearVelocity().x, -13));
    mixer.play(jumpSound);
    timer.delay(0.1, [=](){ canjump = true; });
  }
}

void Jojo::run(int dir) {
  lookdir = dir < 0;
  body->ApplyForceToCenter(b2Vec2 { double(dir) * ((footCounter.getCount()>0)?50:30), -5 }, true);
}

void Jojo::draw() const {
  auto const& pos = body->GetPosition();
  sprite.draw(pos.x * 16, pos.y * 16, lookdir);
}
