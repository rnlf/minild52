#pragma once

#include <queue>
#include <functional>

struct TimerEvent {
  double timeout;
  std::function<void()> event;

  TimerEvent(double timeout, std::function<void()> event);
  TimerEvent(TimerEvent&& old);
  TimerEvent& operator=(TimerEvent&& old);
};

bool operator>(TimerEvent const& a, TimerEvent const& b);

class Timer {
  double currentTime = 0.0;
  std::priority_queue<TimerEvent, std::vector<TimerEvent>, std::greater<TimerEvent>> events;
public:
  void update(double dt);

  void delay(double time, std::function<void()> event);

};
