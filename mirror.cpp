#include "log.hpp"
#include "mirror.hpp"
#include "level.hpp"
#include "loadsprite.hpp"


struct DirSettings {
  std::string anim;
  bool flipX;
  bool flipY;
};

const DirSettings directions[] = {
  {"8", true,  false}, // 0
  {"1", false, false},
  {"6", true,  false},
  {"5", true,  false},
  {"4", false, false}, // 4
  {"5", false, false},
  {"6", false, false},
  {"1", true,  false},
  {"8", false, false}, // 8
  {"1", true,  true },
  {"6", false, true },
  {"5", false, true },
  {"4", false, true }, // 12
  {"5", true,  true },
  {"6", true,  true },
  {"1", false, true }
};

void Mirror::draw() const {
  sprite.draw(x, y, directions[dir].flipX, directions[dir].flipY);
}


Mirror::Mirror(Level& level, int x, int y) 
  : Entity(level, 1)
  , x(x) 
  , y(y) 
  , influence(x / 16, y / 16) {

  sprite = loadSprite("data/sprite/mirror.spr");
  sprite.play("6");

  level.addLaserInfluencer(&influence);
  setState(0);
}


void Mirror::setProperty(std::string property, std::string value) {
  if(property == "state" || property == "dir")
    setState(atoi(value.c_str()));
}

void Mirror::setState(int value) {
  dir = value;
  sprite.play(directions[dir].anim);
  influence.dir = value;
}


void Mirror::update(double dt) {
}
