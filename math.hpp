#pragma once

#include "vector.hpp"

inline bool pointInLine(vec2 const& p, vec2 const& l1, vec2 const& l2) {
  vec2 d = l2 - l1;
  double lsq = dot(p - l1, d);

  if(lsq < 0.0)
    return false;

  return lsq <= d.lensq();
}

inline bool rayRayIntersection(vec2 const& l11, vec2 const& l12, vec2 const& l21, vec2 const& l22, vec2 &result) {
  double den = (l11.x - l12.x) * (l21.y - l22.y) - (l11.y - l12.y) * (l21.x - l22.x);

  if(fabs(den) < 0.00001)
    return false;

  double a = (l11.x*l12.y - l11.y*l12.x);
  double b = (l21.x*l22.y - l21.y*l22.x);
  double px = (a*(l21.x-l22.x) - (l11.x-l12.x)*b)/den;
  double py = (a*(l21.y-l22.y) - (l11.y-l12.y)*b)/den;

  result = {px, py};
  return true;
}


inline bool lineLineIntersection(vec2 const& l11, vec2 const& l12, vec2 const& l21, vec2 const& l22, vec2 &result) {
  vec2 rrp;
  bool rInt = rayRayIntersection(l11, l12, l21, l22, rrp);
  if(!rInt)
    return false;

  if(pointInLine(rrp, l11, l12) && pointInLine(rrp, l21, l22)) {
    result = rrp;
    return true;
  }

  return false;
}

inline bool aabbLineIntersection(vec2 const& l1, vec2 const& l2, vec2 const& aabb1, vec2 const& aabb2) {
  vec2 res;
  return lineLineIntersection(l1, l2, aabb1, aabb1 + vec2(aabb2.x, 0), res)
    || lineLineIntersection(l1, l2, aabb1, aabb1 + vec2(0, aabb2.y), res)
    || lineLineIntersection(l1, l2, aabb1 + aabb2, aabb1 + vec2(aabb2.x, 0), res)
    || lineLineIntersection(l1, l2, aabb1 + aabb2, aabb1 + vec2(0, aabb2.y), res);
}

inline vec2 projectPointToRay(vec2 const& p, vec2 const& rs, vec2 const& d) {
  return rs + dot(p-rs, normalize(d)) * normalize(d);
}

inline vec2 pointLineDistance(vec2 const& p, vec2 const& l1, vec2 const& l2) {
  return p - projectPointToRay(p, l1, l2-l1);
}

