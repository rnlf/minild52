#include "gate.hpp"
#include "draw.hpp"

#include "level.hpp"
#include "loadimage.hpp"

#include <Box2D/Box2D.h>

Gate::Gate(Level& level, int x, int y)
  : Entity(level)
  , surface(loadImage("data/image/gate.png"))
  , x1(x)
  , y1(y) 
  , blocker(x,y) {
  
  level.addLaserInfluencer(&blocker);
}


void Gate::makeBody() {
  b2BodyDef bodyDef;
  bodyDef.type = b2_staticBody;
  bodyDef.position.Set(float(x1) / 16.0f, float(y1) / 16.0);
  body = level.world.CreateBody(&bodyDef);

  b2PolygonShape box;
  box.SetAsBox((x2-x1) / 32.0, (y2-y1) / 32.0, b2Vec2((x2-x1) / 32.0, (y2-y1) / 32.0), 0);
  b2FixtureDef fixtureDef;
  fixtureDef.shape = &box;
  fixture = body->CreateFixture(&fixtureDef);

  blocker.fixture = fixture;
}

void Gate::draw() const {
  switch(direction) {
  case Direction::DOWN:
    {
      int opening = (y2 - y1) * open;
      int rest    = y2 - y1 - opening;
      for(int step = 0; step < (rest+15) / 16; ++step) {
        blit(*surface, 
              x1, y1 + opening + 16 * step,
              (step == 0 ? 3 : 4) * 16, 0, 16,
              (rest - step * 16) >= 16 ? 16 : (rest - step * 16),
              alphaTest);
      }
      break;
    }

  case Direction::UP:
    {
      int opening = (y2 - y1) * open;
      int rest    = y2 - y1 - opening;
      for(int step = 0; step < (rest+15) / 16; ++step) {
        blit(*surface, 
              // dst
              x1,
              y2 - (opening + 16 * step) - ((rest - step * 16) >= 16 ? 16 : (rest-step*16)),

              // srcp
              (step == 0 ? 5 : 4) * 16, 
              16 - ((rest - step * 16) >= 16 ? 16 : (rest-step*16)),
              // srcs
              16,
              (rest - step * 16) >= 16 ? 16 : (rest-step*16),

              alphaTest);
      }
      break;
    }

  case Direction::RIGHT:
    {
      int opening = (x2 - x1) * open;
      int rest    = x2 - x1 - opening;
      for(int step = 0; step < (rest+15) / 16; ++step) {
        blit(*surface, 
              x1 + opening + 16 * step,
              y1,
              (step == 0 ? 0 : 1) * 16,
              0,
              (rest - step * 16) >= 16 ? 16 : (rest - step * 16),
              16,
              alphaTest);
      }
      break;
    }

  case Direction::LEFT:
    {
      int opening = (x2 - x1) * open;
      int rest    = x2 - x1 - opening;
      for(int step = 0; step < (rest+15) / 16; ++step) {
        blit(*surface, 
              // dst
              x2 - (opening + 16 * step) - ((rest - step * 16) >= 16 ? 16 : (rest-step*16)),
              y1,

              // srcp
              (step == 0 ? 3 : 2) * 16 - ((rest - step * 16) >= 16 ? 16 : (rest-step*16)),
              0,
              // srcs
              (rest - step * 16) >= 16 ? 16 : (rest-step*16),
              16,

              alphaTest);
      }
      break;
    }

  };
}

void Gate::update(double dt) {
  tween.update(dt);
}

void Gate::setProperty(std::string property, std::string value) {
  if(property == "dir") {
    if(value == "up")
      setDirection(Direction::UP);
    else if(value == "down")
      setDirection(Direction::DOWN);
    else if(value == "left")
      setDirection(Direction::LEFT);
    else if(value == "right")
      setDirection(Direction::RIGHT);
  } else if(property == "state") {
    setState(value == "on");
  } else if(property == "time") {
    setTime(atof(value.c_str()));
  }
}

void Gate::setDirection(Direction dir) {
  direction = dir;
}

void Gate::updatePhysics() {
  int nx = x1,
      ny = y1;

  switch(direction) {
  case Direction::UP:
    ny = y1 - (y2 - y1) * open;
    break;
  case Direction::DOWN:
    ny = y1 + (y2 - y1) * open;
    break;
  case Direction::LEFT:
    nx = x1 - (x2 - x1) * open;
    break;
  case Direction::RIGHT:
    nx = x1 + (x2 - x1) * open;
    break;
  };

  body->SetTransform(b2Vec2(nx /16.0f, ny /16.0f), 0);
}


void Gate::setState(bool state) {
  if(state) {
    tween.to(time, &open, 1.1f)
      .onupdate([=](){updatePhysics();});
  } else {
    tween.to(time, &open, 0.0f)
      .onupdate([=](){updatePhysics();});
  }
}

void Gate::setTime(float time) {
  this->time = time;
}
