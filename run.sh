#!/bin/bash

./make.sh

truncate -s 0 $PWD/build/LOG.TXT
/usr/bin/dosbox -conf /home/rnlf/.dosbox/dosbox-0.74.conf -c "mount c $PWD/build" -c "c:" -c "minild52.exe" &
tail -f $PWD/build/LOG.TXT
