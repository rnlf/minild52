#include "log.hpp"
#include "statemanager.hpp"

StateManager::StateManager() {
}

void StateManager::process(double dt) {
  if(currentState != nextState) {
    currentState = nextState;
  }

  if(currentState) {
    currentState->update(dt);
    currentState->draw();
  }
}

void StateManager::switchState(std::shared_ptr<Gamestate> next) {
  nextState = next;
}

