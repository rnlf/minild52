#include <cstdint>
#include <dpmi.h>
#include <go32.h>
#include <pc.h>
#include <dos.h>
#include <cstring>
#include "keyboard.hpp"

static uint8_t kbdInputQueue[128];
static int kbdQueueWritePos;

static void kbdIrqHandler() {
  if(kbdQueueWritePos == 128)
    return;

  kbdInputQueue[kbdQueueWritePos] = inp(0x60);
  ++kbdQueueWritePos;

  outp(0x20, 0x20);
}


static _go32_dpmi_seginfo oldKbdHandler, newKbdHandler;

void keyboardInit() {
  _go32_dpmi_get_protected_mode_interrupt_vector(9, &oldKbdHandler);
  newKbdHandler.pm_offset = (int)kbdIrqHandler;
  newKbdHandler.pm_selector = _go32_my_cs();
  _go32_dpmi_allocate_iret_wrapper(&newKbdHandler);
  _go32_dpmi_set_protected_mode_interrupt_vector(9, &newKbdHandler);
}

void keyboardRelease() {
  _go32_dpmi_set_protected_mode_interrupt_vector(9, &oldKbdHandler);
  _go32_dpmi_free_iret_wrapper(&newKbdHandler);
}

static uint8_t kbdShadowQueue[128];
static bool    keysDown[0x200];
static bool    keysPressed[0x200];
static bool    keysReleased[0x200];

void keyboardPump() {
  disable();
  memcpy(kbdShadowQueue, kbdInputQueue, kbdQueueWritePos);
  memset(kbdInputQueue, 0, kbdQueueWritePos);
  int kbdQueueLen = kbdQueueWritePos;
  kbdQueueWritePos = 0;

  // If only first byte of two-byte scancode was received,
  // keep the first byte in the input queue
  if(kbdShadowQueue[kbdQueueLen-1] == 0xE0) {
    kbdInputQueue[0] = 0xE0;
    kbdQueueWritePos = 1;
    --kbdQueueLen;
  }
  enable();

  memset(keysPressed, 0, sizeof(keysPressed));
  memset(keysReleased, 0, sizeof(keysReleased));

  for(int i = 0; i < kbdQueueLen; ++i) {
    int scancode;
    if(kbdShadowQueue[i] == 0xE0) {
      ++i;
      scancode = 0x100 + (kbdShadowQueue[i] & 0x7F);
    } else {
      scancode = kbdShadowQueue[i] & 0x7F;
    }

    bool released = (kbdShadowQueue[i] & 0x80) != 0;

    if(released && keysDown[scancode])
      keysReleased[scancode] = true;

    if(!released && !keysDown[scancode])
      keysPressed[scancode] = true;

    keysDown[scancode] = !released;
  }
}

bool keyboardPressed(int scancode) {
  return keysPressed[scancode];
}

bool keyboardReleased(int scancode) {
  return keysReleased[scancode];
}

bool keyboardDown(int scancode) {
  return keysDown[scancode];
}
