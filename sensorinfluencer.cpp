#include "sensorinfluencer.hpp"
#include "sensor.hpp"

SensorInfluencer::SensorInfluencer(Sensor& sensor, int x, int y)
  : LaserInfluencer(x,y)
  , sensor(sensor) {}

void SensorInfluencer::influence(int& dir, int, int) {
  sensor.hitThisFrame = true;
}
