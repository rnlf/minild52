#include <cmath>
#include "linetrace.hpp"

std::vector<std::pair<int,int>> traceline(float x0, float y0, float x1, float y1) {
  std::vector<std::pair<int,int>> result;

  int mapX = floor(x0);
  int mapY = floor(y0);
  float dx = x1 - x0;
  float dy = y1 - y0;

  float deltaDistX = sqrt(1 + dy*dy / (dx*dx));
  float deltaDistY = sqrt(1 + dx*dx / (dy*dy));

  int stepX;
  int stepY;

  float sideDistX, sideDistY;

  if(dx < 0.0f) {
    stepX = -1;
    sideDistX = (x0 - mapX) * deltaDistX;
  } else {
    stepX = 1;
    sideDistX = (mapX + 1.0 - x0) * deltaDistX;
  }

  if(dy < 0.0f) {
    stepY = -1;
    sideDistY = (y0 - mapY) * deltaDistY;
  } else {
    stepY = 1;
    sideDistY = (mapY + 1.0 - y0) * deltaDistY;
  }

  result.emplace_back(mapX, mapY);

  int mapLastX = floor(x1);
  int mapLastY = floor(y1);

  while(mapLastY != mapY || mapLastX != mapX) {
    if(sideDistX < sideDistY) {
      sideDistX += deltaDistX;
      mapX += stepX;
    } else {
      sideDistY += deltaDistY;
      mapY  += stepY;
    }

    result.emplace_back(mapX, mapY);
  }

  return result;
}
