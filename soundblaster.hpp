#pragma once

void soundblasterInit(int16_t* (*sampleproc)());
size_t soundblasterNeedSamples();
void soundblasterPassSamples(int16_t* data);
size_t soundblasterGetSamplesPerBuffer();
