#pragma once

#include <cstdlib>
#include <type_traits>

template<size_t bytenum, typename T>
uint8_t byte(T scalar) {
  return (scalar >> (8*bytenum)) & 0xFF;
}

template<typename T>
typename std::underlying_type<T>::type
toUnderlying(T t) {
  return static_cast<typename std::underlying_type<T>::type>(t);
}
