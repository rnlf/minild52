#pragma once

#include "entity.hpp"
#include "genericcontactlistener.hpp"
#include "sound.hpp"
#include <memory>

class b2Body;
class b2Fixture;
class Level;


struct Exit: Entity {
  const static int TYPE = 3;
  bool state = false;
  std::string nextlevel;

  b2Body* body;

  std::shared_ptr<Sound> openSound;
  std::shared_ptr<Sound> closedSound;

  GenericContactListener exitListener;

  Sprite sprite;
  void draw() const override;

  void startContact(b2Fixture* fixture);

  void update(double dt) override;
  Exit(Level& level, int x, int y);

  void setProperty(std::string property, std::string value) override;
  void setState(bool value);
};
