#pragma once

#include "entity.hpp"
#include "contactcounter.hpp"
#include "genericcontactlistener.hpp"
#include "timer.hpp"
#include "jojoinfluencer.hpp"
#include "sound.hpp"

class Level;
class b2Body;
class b2Fixture;

struct Jojo: Entity {
  std::shared_ptr<Sound> jumpSound;
  std::shared_ptr<Sound> deadSound;
  enum class State {
    ALIVE,
    DYING,
    DEAD
  };

  const static int TYPE = 2;
  b2Body* body;
  b2Fixture *foot;
  GenericContactListener jojoContact;
  bool lookdir = false;

  JojoInfluencer influencer;

  Timer timer;

  ContactCounter footCounter;
  State state = State::ALIVE;
  bool canjump = true;

  Sprite sprite;
  void jump();
  void run(int dir);
  void draw() const override;
  void update(double dt) override;
  void kill();


  Jojo(Level& level, int x, int y);
};
