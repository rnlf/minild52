#pragma once

#include <string>
#include "entity.hpp"

struct LogicGate: public Entity {

  std::string function = "and";
  std::string target;
  bool inA = false, inB = false;

  LogicGate(Level& level, int x, int y);

  void setProperty(std::string property, std::string value) override;
  void setInputA(bool value);
  void setInputB(bool value);
  void setFunction(std::string function);
  void updateTargets();

};
