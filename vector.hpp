#pragma once

#include <cmath>

struct vec2 {
  vec2(double x = 0, double y = 0)
    : x(x)
    , y(y) {}

  double x;
  double y;

  double len() const noexcept {
    return sqrt(lensq());
  }

  double lensq() const noexcept {
    return x*x + y*y;
  }
};

inline vec2 operator*(vec2 const& a, vec2 const& b) {
  return vec2(a.x * b.x, a.y * b.y);
}

inline vec2 operator+(vec2 const& a, vec2 const& b) {
  return vec2(a.x + b.x, a.y + b.y);
}

inline vec2 operator-(vec2 const& a, vec2 const& b) {
  return vec2(a.x - b.x, a.y - b.y);
}

inline vec2 operator-(vec2 const& a) {
  return vec2(-a.x, -a.y);
}

inline vec2 normalize(vec2 const& a) {
  double l = a.len();
  return vec2(a.x / l, a.y / l);
}

inline double dot(vec2 const& a, vec2 const& b) {
  return a.x * b.x + a.y * b.y;
}

inline vec2 operator*(double a, vec2 const& b) {
  return vec2(a*b.x, a*b.y);
}

inline vec2 operator*(vec2 const& b, double a) {
  return vec2(b.x*a, b.y*a);
}

inline vec2 scale(vec2 const& a, double l) {
  return normalize(a) * l;
}

inline vec2 normal(vec2 const& a) {
  return vec2(-a.y, a.x);
}

inline vec2& operator+=(vec2& a, vec2 const& b) {
  a.x += b.x;
  a.y += b.y;
  return a;
}

inline vec2 abs(vec2 const& a) {
  return {fabs(a.x), fabs(a.y)};
}
