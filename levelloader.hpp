#pragma once

#include <string>
#include "level.hpp"

Level loadLevel(std::string filename);
