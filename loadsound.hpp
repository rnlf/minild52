#pragma once

#include <memory>
#include <string>
#include "sound.hpp"

std::shared_ptr<Sound> loadSound(std::string name);
