cmake_minimum_required(VERSION 2.8)
project(minild52)

set(CMAKE_TOOLCHAIN_FILE cmake/Toolchain-djgpp.cmake)

set(CMAKE_CXX_FLAGS "-std=c++14 -U__STRICT_ANSI__ -U_POSIX_SOURCE -Wall -pedantic -Wno-attributes -O3")
set(CMAKE_C_FLAGS "-U__STRICT_ANSI__ -U_POSIX_SOURCE -Wall -pedantic")

#include_directories(/usr/local/djgpp/include)
#link_directories(/usr/local/djgpp/lib)

include_directories(ext/lodepng)
include_directories(ext/libtmx-parser/src)
include_directories(ext/libtmx-parser/libs/tinyxml2)
include_directories(ext/Box2D)
include_directories(ext/flux/include)
include_directories(ext/flux/src)

add_subdirectory(ext/Box2D)

add_executable(minild52.exe
  basesprite.cpp
  clock.cpp
  contactcounter.cpp
  dma.cpp
  dosmemory.cpp
  draw.cpp
  dynblock.cpp
  entity.cpp
  entityfactory.cpp
  exit.cpp
  font.cpp
  gamestate.cpp
  gate.cpp
  genericcontactlistener.cpp
  glass.cpp
  ingame.cpp
  jojo.cpp
  jojoinfluencer.cpp
  keyboard.cpp
  laser.cpp
  laserblocker.cpp
  level.cpp
  levelcontactlistener.cpp
  leveldonestate.cpp
  levelloader.cpp
  linetrace.cpp
  loadimage.cpp
  loadsound.cpp
  loadsprite.cpp
  log.cpp
  logicgate.cpp
  main.cpp
  mirror.cpp
  mixer.cpp
  oscillator.cpp
  pngloader.cpp
  pulsecounter.cpp
  sensor.cpp
  sensorinfluencer.cpp
  soundblaster.cpp
  sound.cpp
  sprite.cpp
  spriteloader.cpp
  statemanager.cpp
  surface.cpp
  switch.cpp
  timer.cpp
  vesa.cpp
  video.cpp
  waveloader.cpp

  ext/flux/src/flux.cpp

  ext/lodepng/lodepng.cpp

  ext/libtmx-parser/src/tmxparser.cpp
  ext/libtmx-parser/libs/tinyxml2/tinyxml2.cpp
)

target_link_libraries(minild52.exe
  Box2D
)

install(TARGETS minild52.exe RUNTIME DESTINATION /)
