#include "draw.hpp"
#include "leveldonestate.hpp"
#include "font.hpp"
#include "keyboard.hpp"
#include "ingame.hpp"
#include <memory>
#include "statemanager.hpp"
#include "loadimage.hpp"

extern std::unique_ptr<Font> font;

struct TintedReplace {
  uint32_t tint;
  uint32_t alphamask;
  TintedReplace(uint32_t tint) 
    : tint(tint) 
    , alphamask(tint & 0xFF000000) { }

  inline uint32_t operator()(uint32_t dst, uint32_t src) noexcept {
    return alphaBlend(tint, (src & 0x00ffffff) | alphamask);
  }
};

LevelDoneState::LevelDoneState(StateManager& manager, std::string nextlevel, std::string message) 
  : Gamestate(manager)
  , nextlevel(nextlevel) 
  , screenshot(320,200) 
  , message(loadImage( nextlevel != "data/level/level02.tmx" ? ("data/image/" + message +".png") : "data/image/winscrn.png")) { 
  
  unclippedBlit(screenshot, *getActiveDrawSurface(), 0, 0, 0, 0, 320, 200, replace);
  tween.to(4, &tintalpha, 0x50);
}

void LevelDoneState::update(double dt) {
  tween.update(dt);

  if(keyboardDown(0x1C) || keyboardDown(0x13)) {
    auto a = std::make_shared<Ingame>(manager, nextlevel);
    manager.switchState(a);
  }
}

void LevelDoneState::draw() const {
  unclippedBlit(*getActiveDrawSurface(), screenshot,  0, 0, 0, 0, 320, 200, TintedReplace(tintalpha << 24));
  unclippedBlit(*getActiveDrawSurface(), *message, 160 - message->getWidth() / 2, 100 - message->getHeight() / 2,
    0, 0, message->getWidth(), message->getHeight(), alphaTest);
}

