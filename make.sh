mkdir -p build
pushd build
cmake -DCMAKE_TOOLCHAIN_FILE=../cmake/Toolchain-djgpp.cmake ..
make $1
popd
rsync -r data build
rsync ext/CWSDPMI.EXE build
/usr/local/djgpp/bin/i586-pc-msdosdjgpp-strip -s build/minild52.exe
