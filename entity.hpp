#pragma once

#include "sprite.hpp"
#include "vector.hpp"

class Level;

struct Entity {
  int typeId = 0;
  int layer;

  Level& level;

  Entity(Level& level, int layer = 1) 
    : layer(layer)
    , level(level) {
    
  }

  virtual void update(double dt);
  virtual void draw() const;

  virtual void setProperty(std::string property, std::string value);

  void sendProperty(std::string target, std::string value) const;
};
