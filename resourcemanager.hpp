#pragma once

#include <map>
#include <memory>
#include <string>


template<typename ResourceType, typename ResourceLoadFunction>
class ResourceManager {
public:
  typedef std::weak_ptr<ResourceType> WeakPtr;
  typedef std::shared_ptr<ResourceType> SharedPtr;

private:
  typedef std::map<std::string, WeakPtr> ResourceMap;
  ResourceMap resources;

  ResourceLoadFunction forceload;

public:
  ResourceManager(ResourceLoadFunction f)
    : forceload(f) {}

  SharedPtr load(std::string name) {
    auto it = resources.find(name);
    if(it != resources.end() && !it->second.expired()) {
      return SharedPtr(it->second);
    }

    auto res = forceload(name);
    auto newResource = std::make_shared<ResourceType>(std::move(res));
    resources.emplace(name, newResource);
    return newResource;
  }
};
