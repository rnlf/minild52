#pragma once

class LaserInfluencer {
public:
  int x;
  int y;

  LaserInfluencer(int x, int y)
    : x(x), y(y) {}

  virtual void influence(int& dir, int, int) = 0;
  virtual bool at(int qx, int qy) const {
    return x == qx and y == qy;
  }
};
