#include "ingame.hpp"
#include "levelloader.hpp"

#include "statemanager.hpp"

#include "keyboard.hpp"

Ingame::Ingame(StateManager& manager, std::string levelname) 
  : Gamestate(manager)
  , level(loadLevel(levelname)) {
  level.gamestate = this;
}

void Ingame::update(double dt) {
  if(keyboardDown(0x13)) {
    auto a = std::make_shared<Ingame>(manager, level.levelname);
    manager.switchState(a);
  }
    

  level.update(dt);
}

void Ingame::draw() const {
  level.draw();
}

