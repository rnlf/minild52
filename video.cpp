
#include "surface.hpp"

static Surface* activeDrawSurface;

Surface* getActiveDrawSurface() {
  return activeDrawSurface;
}

void setActiveDrawSurface(Surface* surface) {
  activeDrawSurface = surface;
}
