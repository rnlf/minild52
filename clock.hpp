#pragma once

void clockInit();
void clockUpdate();
double clockDelta();
double clockTime();
double clockFPS();
