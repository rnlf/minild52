#pragma once

#include <vector>
#include <string>
#include "entity.hpp"
#include "timer.hpp"

struct Oscillator: Entity {
  std::vector<std::string> values = {"on", "off"};
  int pulse;
  std::string target;
  Timer timer;
  float frequency = 1.0f;
  bool state = false;

  Oscillator(Level& level, int x, int y);

  void startTimer();
  void postTarget();

  void setProperty(std::string property, std::string value) override;
  void setState(bool value);
  void setPulse(int i);
  void setValues(std::vector<std::string> values);
  void setFrequency(float freq);

  void update(double dt) override;

};
