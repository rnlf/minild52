#include <sstream>
#include "level.hpp"
#include "pulsecounter.hpp"

PulseCounter::PulseCounter(Level& level, int, int) 
  : Entity(level) {
}

void PulseCounter::setProperty(std::string property, std::string value) {
  if(property == "state") {
    setState(value == "on");
  } else if(property == "values") {
    values.clear();
    std::string::size_type i = 0;
    for(;;) {
      std::string::size_type j = value.find(',', i);
      values.push_back(value.substr(i, j-i));
      if(j == std::string::npos)
        break;
      i = j+1;
    }
  } else if(property == "pulse") {
    setPulse(atoi(value.c_str()));
  } else if(property == "target") {
    target = value;
  }
}

void PulseCounter::setState(bool value) {
  if((!currentState) && value) {
    setPulse(pulse+1);
  }

  currentState = value;
}

void PulseCounter::setValues(std::vector<std::string> values) {
  this->values = values;
}

void PulseCounter::setPulse(int i) {
  if(!values.empty()) {
    pulse = i % values.size();
    sendProperty(target, values[pulse]);
  } else {
    pulse = i;
    std::ostringstream n;
    n << pulse;
    sendProperty(target, n.str());
  }
}
