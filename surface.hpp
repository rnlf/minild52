#pragma once

#include <cstdint>
#include <memory>

class Surface {
  std::unique_ptr<std::uint32_t[]> storage;
  unsigned width;
  unsigned height;
public:
  Surface();
  Surface(unsigned width, unsigned height);
  Surface(Surface&& old) = default;
  Surface& operator=(Surface&& old) = default;

  unsigned getWidth() const noexcept;
  unsigned getHeight() const noexcept;
  uint32_t* getBuffer() const noexcept;
  
  Surface(Surface const&) = delete; // very costly, use explcit copy()
  Surface& operator=(Surface const&) = delete;
};
