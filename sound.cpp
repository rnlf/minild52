#include "sound.hpp"

int Sound::getSampleCount() const {
  return sampleCount;
}

int16_t* Sound::getSamplePointer(int offset) const {
  return samples.get() + offset;
}

Sound::Sound(int sampleCount)
  : sampleCount(sampleCount)
  , samples(std::make_unique<int16_t[]>(sampleCount)) { }

Sound::Sound(Sound&& old) 
  : sampleCount(0) {

  std::swap(samples, old.samples);
  std::swap(sampleCount, old.sampleCount);
}
