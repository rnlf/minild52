#include "basesprite.hpp"

SpriteFrame::SpriteFrame(unsigned x, unsigned y, unsigned w, unsigned h)
  : x(x)
  , y(y)
  , w(w)
  , h(h) {}


BaseSprite::BaseSprite(std::shared_ptr<Surface> surface, unsigned frameWidth, unsigned frameHeight, BlitMode mode)
  : surface(surface)
  , frameWidth(frameWidth)
  , frameHeight(frameHeight) 
  , blitMode(mode) {}


SpriteAnimation* BaseSprite::getAnimation(std::string name) {
  if(name == "") {
    return &animations[defaultAnim];
  } else
    return &animations[name];
}


Surface const& BaseSprite::getSurface() const {
  return *surface;
}

unsigned BaseSprite::getWidth() const {
  return frameWidth;
}

unsigned BaseSprite::getHeight() const {
  return frameHeight;
}

BlitMode BaseSprite::getBlitMode() const {
  return blitMode;
}
