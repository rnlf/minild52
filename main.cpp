#include <cstring>
#include <unistd.h>
#include <cstdint>
#include <iostream>
#include <iomanip>
#include <dos.h>
#include <dpmi.h>
#include <sys/nearptr.h>
#include "vesa.hpp"
#include <cmath>
#include <pc.h>
#include <go32.h>
#include <memory>
#include "keyboard.hpp"
#include "soundblaster.hpp"
#include "surface.hpp"
#include "draw.hpp"
#include "video.hpp"
#include "log.hpp"

#include "loadimage.hpp"
#include "loadsound.hpp"

#include "sprite.hpp"

#include "font.hpp"

#include "levelloader.hpp"
#include "loadsprite.hpp"

#include "clock.hpp"

#include "ingame.hpp"
#include "statemanager.hpp"

#include "mixer.hpp"

//std::unique_ptr<Font> font;


Mixer mixer;

int16_t* passSamples() {
  return mixer.getNextBlock();
}

int main() {
  try {
    auto data = std::unique_ptr<int16_t[]>(new
      int16_t[soundblasterGetSamplesPerBuffer()]);

    if(!__djgpp_nearptr_enable()) {
      std::cerr << "Could not enable nearptrs. Sorry." << std::endl;
      return 1;
    }


    soundblasterInit(&passSamples);

    //Level level = loadLevel("data/level/level02.tmx");

    //font = std::make_unique<Font>("data/image/font.png");

    keyboardInit();

    Vesa vesa;

    vesa.setMode(0x10f);

    setActiveDrawSurface(vesa.getSurfacePtr());

    auto fat = loadSprite("data/sprite/fat.spr");

    clockInit();

    //std::unique_ptr<Gamestate> gamestate = std::make_unique<Ingame>("test");
    StateManager statemanager;
    auto state = std::make_shared<Ingame>(statemanager, "data/level/level02.tmx");
    statemanager.switchState(state);

    //mixer.play(theSound);

    for(;;) {
      mixer.mix();
      clockUpdate();

      keyboardPump();

    //  clear(vesa.getSurface(), 0x002d2d2d);

      float dt = clockDelta();
      /*
      level.update(dt);
      level.draw();
      */

      statemanager.process(dt);
      if(keyboardPressed(0x10))
        break;

      //font->printf(0, 0, "FPS: %.0f", clockFPS());
      //font.printf(0,16, "DIFF: %.2f, %.2f", jojo.pos.x - jojo.lastPos.x, jojo.pos.y - jojo.lastPos.y);

      vesa.setPage(0);
    }
    vesa.reset();
    sleep(1);
    keyboardRelease();
  } catch(std::exception& e) {  
    log("%s\n", e.what());
  }

  return 0;
}
