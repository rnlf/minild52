#pragma once

#include "laserinfluencer.hpp"

class Jojo;

class JojoInfluencer: public LaserInfluencer {
  Jojo *jojo;
public:
  JojoInfluencer(Jojo* jojo, int x, int y);

  void influence(int & indir, int x, int y) override;
};

