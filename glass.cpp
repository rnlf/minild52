#include "glass.hpp"
#include "loadsprite.hpp"
#include "level.hpp"

Glass::Glass(Level& level, int x, int y) 
  : Entity(level) {

  b2BodyDef bodyDef;
  bodyDef.type = b2_staticBody;
  bodyDef.position.Set(float(x) / 16.0, float(y) / 16.0);
  body = level.world.CreateBody(&bodyDef);

  b2PolygonShape box;
  box.SetAsBox(0.5f, 0.49f, b2Vec2(0.5 - 1 / 16.0, 0.5 - 0.5f / 16.0f), 0);
  b2FixtureDef fixtureDef;
  fixtureDef.shape = &box;
  body->CreateFixture(&fixtureDef);

  sprite = loadSprite("data/sprite/glass.spr");
  sprite.play("exist");
}

void Glass::draw() const {
  auto pos = body->GetPosition();
  sprite.draw(pos.x * 16,pos.y * 16);
}
