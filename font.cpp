#include <cstdarg>
#include <cstdio>
#include "font.hpp"
#include "video.hpp"
#include "loadimage.hpp"
#include "draw.hpp"

Font::Font(std::string image)
  : bitmap(loadImage(image)) {}

void Font::printf(int x, int y, char const* format, ...) {
  auto dst = std::make_unique<char[]>(2048);
  va_list ap;
  va_start(ap, format);
  int n = vsnprintf(dst.get(), 2048, format, ap);
  va_end(ap);

  int w = bitmap->getWidth() / 16;
  int h = bitmap->getHeight() / 16;

  for(int i = 0; i < n; ++i) {
    int c = dst[i];
    blit(*getActiveDrawSurface(), *bitmap, x + i * (w-1), y,
      (c % 16) * w, (c / 16) * h, w, h, alphaTest);
  }
}
