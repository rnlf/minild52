#include <time.h>
#include "clock.hpp"

static unsigned frame = 0;
static double lastTime;
static double currentTime;
static double delta;
static double fpsStart;
static double lastFPS;

static double getTime() {
  return double(uclock()) * 1e-6;
}

void clockInit() {
  lastTime = getTime();
  currentTime = lastTime;
  fpsStart = lastTime;
  lastFPS = 0.0;
}

void clockUpdate() {
  lastTime = currentTime;
  currentTime = getTime();
  delta = currentTime - lastTime;
  ++frame;
  if(currentTime - fpsStart > 1.0f) {
    lastFPS = double(frame) / (currentTime - fpsStart);
    fpsStart = currentTime;
    frame = 0;
  }
}

double clockDelta() {
  return delta;
}

double clockTime() {
  return currentTime;
}

double clockFPS() {
  return lastFPS;
}

