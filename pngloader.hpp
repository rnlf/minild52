#pragma once

#include <string>
#include "surface.hpp"

Surface loadPNG(std::string filename);
