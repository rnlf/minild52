#pragma once

#include "entity.hpp"
#include "laserblocker.hpp"
#include <memory>
#include "sound.hpp"
class b2Body;
class b2Fixture;

class Level;


struct Laser: Entity {
  b2Body *body;
  
  std::shared_ptr<Sound> laserOnSound;
  std::shared_ptr<Sound> laserOffSound;

  LaserBlocker blocker;
  bool state = false;
  Sprite sprite;
  void draw() const override;
  void update(double dt) override;

  int dir = 0;
  Laser(Level& level, int x, int y);
  void setDir(int dir);

  void setProperty(std::string property, std::string value) override;
  void setState(bool value);

  void traceBeam(int x, int y, int dir);

  struct Line {
    Line(int x1, int y1, int x2, int y2)
      : x1(x1), y1(y1), x2(x2), y2(y2) {}
    int x1, y1, x2, y2;
  };
  std::vector<Line> segments;
  struct Dir {
    int x, y;
    std::string on;
    std::string off;
  };
  static const Dir dirs[8];
};
