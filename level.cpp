#include "log.hpp"
#include "level.hpp"
#include "video.hpp"
#include "draw.hpp"
#include "entity.hpp"
#include <cmath>
#include <algorithm>
#include <iterator>
#include <unordered_set>
#include <vector>
#include "math.hpp"


Level::Level(unsigned width, unsigned height) 
  : backgroundImage(320, 200)
  , width(width)
  , height(height)
  , world(b2Vec2(0, 30))
{
  world.SetContactListener(&contactListener);
  noLaser.push_back(0);
}


void Level::setTileset(std::shared_ptr<Surface> tileset) {
  this->tileset = tileset;
  tilesetRow = tileset->getWidth() / 16; // TODO configurable tile size?
}


void Level::setCenterLayer(std::vector<unsigned>&& tiles) {
  this->tiles = std::move(tiles);
  buildStaticCollisionMesh();

  for(unsigned row = 0; row < height; ++row) {
    for(unsigned col = 0; col < width; ++col) {
      unsigned tileid = this->tiles[row * width + col];
      if(tileid > 0) {
        --tileid;
        centerTiles.push_back({
          col*16,
          row*16,
          16*(tileid % tilesetRow),
          16*(tileid / tilesetRow),
          16, 
          (row == 12) ? 8 : 16});
      }
    }
  }
}

void Level::setFrontLayer(std::vector<unsigned>&& tiles) {
  for(unsigned row = 0; row < height; ++row) {
    for(unsigned col = 0; col < width; ++col) {
      unsigned tileid = tiles[row * width + col];
      if(tileid > 0) {
        --tileid;
        frontTiles.push_back({
          col*16,
          row*16,
          16*(tileid % tilesetRow),
          16*(tileid / tilesetRow),
          16, 
          (row == 12) ? 8 : 16});
      }
    }
  }
}

void Level::setBackgroundLayer(std::vector<unsigned>&& tiles) {
  this->background = std::move(tiles);
  for(unsigned row = 0; row < height; ++row) {
    for(unsigned col = 0; col < width; ++col) {
      unsigned tileid = background[row * width + col] - 1;
      blit(backgroundImage, *tileset, col*16, row*16,
        16*(tileid % tilesetRow), 16*(tileid / tilesetRow), 16, 16, replace);
    }
  }
}


void Level::draw() const {
  unclippedBlit(*getActiveDrawSurface(), backgroundImage, 0, 0, 0, 0, 320, 200, replace);

  for(auto const& ent: layers[0])
    ent->draw();

  for(auto const& t: centerTiles) {
    unclippedBlit(*getActiveDrawSurface(), *tileset, t.dstx, t.dsty, t.srcx, t.srcy, t.srcw, t.srch, alphaTest);
  }

  for(auto const& ent: layers[1])
    ent->draw();

  for(auto const& ent: layers[2])
    ent->draw();

  for(auto const& t: frontTiles) {
    unclippedBlit(*getActiveDrawSurface(), *tileset, t.dstx, t.dsty, t.srcx, t.srcy, t.srcw, t.srch, alphaBlend);
  }
}

void Level::update(double dt) {
  world.Step(dt, 16, 12);
  for(auto & ent: entities) {
    ent->update(dt);
  }
}

int Level::getTile(int col, int row) const noexcept {
  if(col < 0 || col >= width || row < 0 || row >= height)
    return -1;
  return tiles[row * width + col];
}

void Level::addStaticLine(float x1, float y1, float x2, float y2) {
  b2BodyDef groundBodyDef;
  groundBodyDef.type = b2_staticBody;
  groundBodyDef.position.Set(0.0f, 0.0f);
  b2Body* groundBody = world.CreateBody(&groundBodyDef);
  b2EdgeShape edge;
  edge.Set(b2Vec2(x1 / 16.0f, y1 / 16.0f), b2Vec2(x2 / 16.0f,y2 / 16.0f));
  groundBody->CreateFixture(&edge, 0.0f)->SetFriction(1);
}

void Level::buildStaticCollisionMesh() {
  for(int row = 0; row < height; ++row) {
    int firstcolabove = -1;
    int firstcolbelow = -1;
    for(int col = 0; col < width; ++col) {
      if(getTile(col, row) > 0 && getTile(col, row -1) <= 0) {
        if(firstcolbelow == -1) {
          firstcolbelow = col;
        }
      } else {
        if(firstcolbelow != -1) {
          addStaticLine(firstcolbelow * 16, row * 16, col * 16 - 1, row * 16);
          firstcolbelow = -1;
        }
      }

      if(getTile(col, row) > 0 && getTile(col, row + 1) <= 0) {
        if(firstcolabove == -1) {
          firstcolabove = col;
        }
      } else {
        if(firstcolabove != -1) {
          addStaticLine(firstcolabove * 16, row * 16 + 15, col * 16 - 1, row * 16 + 15);
          firstcolabove = -1;
        }
      }
    }
  }

  for(int col = 0; col < width; ++col) {
    int firstrowleft = -1;
    int firstrowright = -1;
    for(int row = 0; row < height; ++row) {
      if(getTile(col, row) > 0 && getTile(col + 1, row) <= 0) {
        if(firstrowright == -1) {
          firstrowright = row;
        }
      } else {
        if(firstrowright != -1) {
          addStaticLine(col * 16 + 15, firstrowright * 16, col * 16 + 15, row * 16 - 1);
          firstrowright = -1;
        }
      }

      if(getTile(col, row) > 0 && getTile(col - 1, row) <= 0) {
        if(firstrowleft == -1) {
          firstrowleft = row;
        }
      } else {
        if(firstrowleft != -1) {
          addStaticLine(col * 16, firstrowleft * 16, col * 16, row * 16 - 1);
          firstrowleft = -1;
        }
      }
    }
  }
}

void Level::addEntity(std::unique_ptr<Entity>&& entity, std::string name) {
  entities.emplace_back(std::move(entity));
  if(!name.empty()) {
    namedEntities.emplace(name, entities.back().get());
  }

  if(entities.back()->layer != -1)
    layers[entities.back()->layer].push_back(entities.back().get());
}

Entity* Level::getNamedEntity(std::string name) const {
  auto it = namedEntities.find(name);
  if(it != namedEntities.end())
    return it->second;
  return nullptr;
}

void Level::addLaserInfluencer(LaserInfluencer* influencer) {
  auto it = find(laserInfluencers.begin(), laserInfluencers.end(), influencer);
  if(it == laserInfluencers.end())
    laserInfluencers.push_back(influencer);
}

void Level::removeLaserInfluencer(LaserInfluencer* influencer) {
  auto it = find(laserInfluencers.begin(), laserInfluencers.end(), influencer);
  if(it != laserInfluencers.end())
    laserInfluencers.erase(it);
}

std::vector<LaserInfluencer*> Level::getLaserInfluencersAt(int x, int y) const {
  std::vector<LaserInfluencer*> ret;
  auto it = copy_if(laserInfluencers.begin(), laserInfluencers.end(), 
                    std::back_inserter(ret), [=](auto e) {return e->at(x,y);});
  return ret;
}

void Level::addNoLaser(int idx) {
  noLaser.push_back(idx);
}

bool Level::blocksLaser(int col, int row) const noexcept {
  return find(noLaser.begin(), noLaser.end(), getTile(col, row)) == noLaser.end();
}
