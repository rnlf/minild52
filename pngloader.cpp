
#include "pngloader.hpp"
#include <lodepng.h>
#include <cstring>
#include <cstdlib>


static void pixelCopySwapRB(uint32_t *dst, uint32_t const* src, size_t pixelcount) {
  for(size_t i = 0; i < pixelcount; ++i) {
    unsigned b = src[i];
    unsigned int x = (b ^ (b >> 16)) & ((1U << 8) - 1);
    dst[i] = b ^ (x | (x << 16));
  }
}

Surface loadPNG(std::string filename) {
  unsigned char *data;
  unsigned w, h;
  lodepng_decode32_file(&data, &w, &h, filename.c_str());

  Surface surface(w, h);
  pixelCopySwapRB(surface.getBuffer(), reinterpret_cast<uint32_t*>(data), w*h);

  free(data);

  return surface;
}
