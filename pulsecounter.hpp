#pragma once

#include <vector>
#include <string>
#include "entity.hpp"

struct PulseCounter: Entity {
  std::vector<std::string> values;
  int pulse = 0;
  bool currentState = false;
  std::string target;

  PulseCounter(Level& level, int x, int y);

  void setProperty(std::string property, std::string value) override;
  void setState(bool value);
  void setPulse(int i);
  void setValues(std::vector<std::string> values);


};
