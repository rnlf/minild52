#pragma once

#include <algorithm>
#include <cstring>
#include "surface.hpp"
#include "tools.hpp"
#include "video.hpp"

enum class BlitMode {
  REPLACE,
  ALPHATEST,
  ALPHABLEND
};


void clear(Surface& dst, uint32_t color);

inline uint32_t replace(uint32_t const dst, uint32_t const src) noexcept {
  return (src & 0xFFFFFF);
}


inline uint32_t alphaTest(uint32_t const dst, uint32_t const src) noexcept {
  return byte<3>(src) > 0x80 ? (src & 0xFFFFFF) : dst;
}


inline uint32_t alphaBlend(uint32_t dst, uint32_t const src) noexcept {
  //for(size_t x = 0; x < count; ++x) {
#define ALPHABLEND_MODE 0

#if ALPHABLEND_MODE == 0

    uint8_t sa = src >> 24;
    uint8_t da = 255 ^ sa;

    int32_t sg = (src & 0xFF00) >> 8;
    int32_t dg = (dst & 0xFF00) >> 8;
    uint32_t ng = (uint32_t(dg + (((sg - dg) * int32_t(sa))>>8)));

    uint32_t nrb = (dst & 0xFF00FFUL) * da + (src & 0xFF00FFUL) * sa;
    return (ng << 8)
           | ((nrb & 0xFF00FF00UL) >> 8);
#elif ALPHABLEND_MODE == 1
    uint32_t d = dst[x];
    uint32_t s = src[x];

    uint8_t sa = s >> 24;
    uint8_t da = 255 ^ sa;

    uint32_t nrb = (d & 0xFF00FFUL) * da + (s & 0xFF00FFUL) * sa;
    uint32_t ng =  (d & 0x00FF00UL) * da + (s & 0x00FF00UL) * sa;
    dst[x] = ((ng & 0x00FF0000UL)
           | (nrb & 0xFF00FF00UL)) >> 8;

#else
    uint32_t d = dst[x];
    uint32_t s = src[x];

    uint8_t sa = s >> 24;
    uint8_t da = 255 ^ sa;

    uint32_t nrb = (d & 0xFF00FFUL) * da + (s & 0xFF00FFUL) * sa;
    uint32_t ng =  (d & 0x00FF00UL) * da + (s & 0x00FF00UL) * sa;
    dst[x] = (((ng  & 0x00FFFF00UL) / 255) & 0x0000FF00UL)
           | (((nrb & 0xFFFF0000UL) / 255) & 0x00FF0000UL)
           | (((nrb & 0x0000FFFFUL) / 255) & 0x000000FFUL);
#endif
  //}
}

bool clipSourceAndDestination(
  Surface const& dst, Surface const& src, int &dstx, int &dsty,
  int &srcx, int &srcy, int &srcw, int &srch) noexcept;


template<typename BlitFunctor>
void unclippedBlit(Surface& dst, Surface const& src, unsigned dstx,
                 unsigned dsty, unsigned srcx, unsigned srcy, unsigned srcw,
                 unsigned srch, BlitFunctor blitFunctor, bool flipx=false, bool flipy=false) {

  uint32_t *dstptr = dst.getBuffer() + (dsty + (flipy ? srch - 1 : 0)) * dst.getWidth() + dstx;
  uint32_t *srcptr = src.getBuffer() + srcy * src.getWidth() + srcx + (flipx ? srcw - 1 : 0);

  signed dststep = (flipy ? -1 : 1) * dst.getWidth() - srcw;
  signed srcstep = src.getWidth() + (flipx ? 1 : -1) * srcw;

  signed srcpixelstep = flipx ? -1 : 1;


  


  for(unsigned r = 0; r < srch; ++r) {
    for(unsigned c = 0; c < srcw ; ++c) {
      *dstptr = blitFunctor(*dstptr, *srcptr);
      ++dstptr;
      srcptr += srcpixelstep;
    }
    dstptr += dststep;
    srcptr += srcstep;
  }
}



template<typename BlitFunctor>
void blit(Surface& dst, Surface const& src, int dstx, int dsty,
                 int srcx, int srcy, int srcw, int srch, 
                 BlitFunctor blitFunctor, bool flipx = false, bool flipy = false) {
  if(!clipSourceAndDestination(dst, src, dstx, dsty, srcx, srcy, srcw, srch))
    return;

  unclippedBlit(dst, src, dstx, dsty, srcx, srcy, srcw, srch, blitFunctor, flipx, flipy);
}

template<typename BlitFunctor>
void blit(Surface const& src, int dstx, int dsty,
                 int srcx, int srcy, int srcw, int srch, 
                 BlitFunctor blitFunctor, bool flipx = false, bool flipy = false) {
  blit(*getActiveDrawSurface(), src, dstx, dsty, srcx, srcy, srcw, srch, blitFunctor, flipx, flipy);
}

void blit(Surface const& src, int dstx, int dsty,
                 int srcx, int srcy, int srcw, int srch, 
                 BlitMode mode, bool flipx = false, bool flipy = false);

void blit(Surface& dst, Surface const& src, int dstx, int dsty,
                 int srcx, int srcy, int srcw, int srch, 
                 BlitMode mode, bool flipx = false, bool flipy = false);

template<typename BlitFunctor>
void line(Surface& dst, int x0, int y0, int x1, int y1, uint32_t color, BlitFunctor blitFunctor) {
  // Stolen from Wikipedia

  int dx =  abs(x1-x0), sx = x0<x1 ? 1 : -1;
  int dy = -abs(y1-y0), sy = y0<y1 ? 1 : -1;
  int err = dx+dy, e2;
 
  uint32_t *buffer = dst.getBuffer();
  int w = dst.getWidth();

  for(;;) {
    // TODO proper line clipping
    if(x0 >= 0 && x0 < dst.getWidth() && y0 >= 0 && y0 < dst.getHeight()) {
      uint32_t *pixel = buffer  + y0 * w + x0;
      *pixel = blitFunctor(*pixel, color);
    }

    if (x0==x1 && y0==y1) break;
    e2 = 2*err;
    if (e2 > dy) { err += dy; x0 += sx; }
    if (e2 < dx) { err += dx; y0 += sy; }
  }
}

template<typename BlitFunctor>
void line(int x0, int y0, int x1, int y1, uint32_t color, BlitFunctor blitFunctor) {
  line(*getActiveDrawSurface(), x0, y0, x1, y1, color, blitFunctor);

}

