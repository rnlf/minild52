#include "loadimage.hpp"
#include "resourcemanager.hpp"
#include "pngloader.hpp"


static ResourceManager<Surface, Surface (*)(std::string)> surfaceManager(loadPNG);

std::shared_ptr<Surface> loadImage(std::string name) {
  return surfaceManager.load(name);
}

