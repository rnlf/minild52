#pragma once

#include <utility>
#include <vector>

std::vector<std::pair<int,int>> traceline(float x0, float y0, float x1, float y1);
