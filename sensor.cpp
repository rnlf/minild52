#include "sensor.hpp"
#include "level.hpp"
#include "loadsprite.hpp"
#include "draw.hpp"

#include <Box2D/Box2D.h>

void Sensor::draw() const {
  auto pos = body->GetPosition();
  sprite.draw(pos.x * 16,pos.y * 16);
}



Sensor::Sensor(Level& level, int x, int y) 
  : Entity(level, 1)
  , influencer(*this, x / 16, y / 16) {
  b2BodyDef bodyDef;
  bodyDef.type = b2_staticBody;
  bodyDef.position.Set(float(x) / 16.0, float(y) / 16.0);
  body = level.world.CreateBody(&bodyDef);

  b2PolygonShape box;
  box.SetAsBox(0.5f, 0.49f, b2Vec2(0.5 - 1 / 16.0, 0.5 - 0.5f / 16.0f), 0);
  b2FixtureDef fixtureDef;
  fixtureDef.shape = &box;
  body->CreateFixture(&fixtureDef);

  sprite = loadSprite("data/sprite/sensor.spr");
  sprite.play("off");

  level.addLaserInfluencer(&influencer);
}



void Sensor::setProperty(std::string property, std::string value) {
  if(property == "target")
    setTarget(value);
}

void Sensor::setTarget(std::string target) {
  this->target = target;
}

void Sensor::update(double dt) {
  hitLastFrame = hitThisFrame;
  hitThisFrame = false;

  bool isOn = hitThisFrame || hitLastFrame;

  sprite.play( isOn ? "on" : "off");

  if(isOn != wasOn)
    sendProperty(target, isOn ? "on" : "off");

  wasOn = isOn;
}
