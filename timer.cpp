#include <utility>
#include "timer.hpp"

TimerEvent::TimerEvent(double timeout, std::function<void()> event)
  : timeout(timeout)
  , event(event) {}

TimerEvent::TimerEvent(TimerEvent&& old) {
  std::swap(timeout, old.timeout);
  std::swap(event, old.event);
}

TimerEvent& TimerEvent::operator=(TimerEvent&& old) {
  std::swap(timeout, old.timeout);
  std::swap(event, old.event);
}

bool operator>(TimerEvent const& a, TimerEvent const& b) {
  return a.timeout > b.timeout;
}

void Timer::delay(double time, std::function<void()> event) {
  events.emplace(time+currentTime, event);
}

void Timer::update(double dt) {
  currentTime += dt;
  while(!events.empty() && events.top().timeout <= currentTime) {
    events.top().event();
    events.pop();
  }
}
