#pragma once

#include <memory>
#include <string>
#include "surface.hpp"

std::shared_ptr<Surface> loadImage(std::string name);

