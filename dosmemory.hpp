#pragma once

#include <cstdlib>
#include <new>
#include <type_traits>
#include <utility>
#include <dpmi.h>
#include <map>
#include <sys/nearptr.h>

class DOSAllocationError: public std::bad_alloc { };
class PhysicalMemoryMapError: public std::runtime_error {
public:
  PhysicalMemoryMapError()
    : std::runtime_error("Failed to map physical memory") {}
};

template<typename T>
class FarPtr {
  uint16_t offset;
  uint16_t segment;

public:
  FarPtr() {} // allow uninitialized FarPtrs
  
  FarPtr(int segment, int offset = 0)
    : offset(offset)
    , segment(segment)
  {}

  FarPtr(uint16_t farptr[2])
    : FarPtr(farptr[1], farptr[0])
  {}

  T* get() const noexcept {
    return  reinterpret_cast<T*>(
        __djgpp_conventional_base
        + segment * 16 + offset);
  }

  typename std::add_lvalue_reference<T>::type
  operator*() const noexcept {
    return *get();
  }

  T* operator->() const noexcept {
    return get();
  }

  int getSegment() const noexcept {
    return segment; 
  }

  int getOffset() const noexcept {
    return offset; 
  }

  typename std::add_lvalue_reference<T>::type
  operator[](size_t i) {
    return get()[i];
  }

};

static std::map<int, int> dosAllocSegmentToSelector;

FarPtr<void> dosMalloc(size_t size);

template<typename T, typename... Args>
FarPtr<T> dosNew(Args... args) {
  int selector;
  int segment = __dpmi_allocate_dos_memory((sizeof(T)+15)/16, &selector);

  if(segment == -1) {
    throw DOSAllocationError();
  }

  dosAllocSegmentToSelector[segment] = selector;

  FarPtr<T> ptr {segment, 0};
  new (ptr.get()) T(args...);

  return ptr;
}

template<typename T>
void dosDelete(FarPtr<T> const& ptr) noexcept {
  auto it = dosAllocSegmentToSelector.find(ptr.getSegment());
  if(it != dosAllocSegmentToSelector.end()) {
    __dpmi_free_dos_memory(it->second);
    dosAllocSegmentToSelector.erase(it);
  }
}


template<typename T>
class UniqueFarPtr {
  FarPtr<T> ptr;
public:
  UniqueFarPtr()
    : ptr(0,0) {}

  UniqueFarPtr(FarPtr<T> ptr)
    : ptr(ptr) {}

  ~UniqueFarPtr() {
    dosDelete(ptr);
  }

  UniqueFarPtr(UniqueFarPtr<T>&& old) {
    std::swap(old.ptr, ptr);
  }

  UniqueFarPtr(UniqueFarPtr<T> const&) = delete;

  UniqueFarPtr<T>& operator=(UniqueFarPtr&& old) {
    std::swap(old.ptr, ptr);
    return *this;
  }

  UniqueFarPtr<T>& operator=(UniqueFarPtr const& old) = delete;

  FarPtr<T> const& get() const noexcept {
    return ptr;
  }

  typename std::add_lvalue_reference<T>::type
  operator*() const noexcept {
    return *get();
  }

  T* operator->() const noexcept {
    return get().get();
  }

  typename std::add_lvalue_reference<T>::type
  operator[](size_t i) {
    return get()[i];
  }
};

template<typename T, typename... Args>
UniqueFarPtr<T> makeUniqueFar(Args... args) {
  return UniqueFarPtr<T>(dosNew<T, Args...>(args...));
}


template<typename T>
class MappedPhysicalMemory {
  __dpmi_meminfo meminfo;
  T* ptr;
public:
  MappedPhysicalMemory()
    : ptr(nullptr) {}

  MappedPhysicalMemory(uint32_t baseaddr, uint32_t size) 
    : MappedPhysicalMemory() {

    meminfo.address = baseaddr;
    meminfo.size = size;
    if(__dpmi_physical_address_mapping(&meminfo) == -1)
      throw PhysicalMemoryMapError();

    ptr = reinterpret_cast<T*>(meminfo.address + __djgpp_conventional_base);
  }

  ~MappedPhysicalMemory() noexcept {
    if(ptr)
      __dpmi_free_physical_address_mapping(&meminfo);
  }

  MappedPhysicalMemory(MappedPhysicalMemory&& old) 
    : MappedPhysicalMemory() {

    meminfo = old.meminfo;
    std::swap(ptr, old.ptr);
  }

  MappedPhysicalMemory& operator=(MappedPhysicalMemory&& old) {
    std::swap(ptr, old.ptr);
    std::swap(meminfo, old.meminfo);
    return *this;
  }

  MappedPhysicalMemory(MappedPhysicalMemory const&) = delete;
  MappedPhysicalMemory& operator=(MappedPhysicalMemory const&) = delete;

  T* get() const noexcept {
    return ptr;
  }
};
