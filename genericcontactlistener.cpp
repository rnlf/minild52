#include "genericcontactlistener.hpp"

#include <Box2D/Box2D.h>

void GenericContactListener::beginContact(b2Fixture *fixture) {
  if(beginContactFunc)
    beginContactFunc(fixture);
}

void GenericContactListener::endContact(b2Fixture *fixture) {
  if(endContactFunc)
    endContactFunc(fixture);
}
