#pragma once

#include "entity.hpp"


#include "mirrorinfluence.hpp"
class Level;

struct Mirror: Entity {
  int x;
  int y;
  
  MirrorInfluence influence;

  int dir = 0;
  float f = 0.0f;

  Sprite sprite;

  void draw() const override;

  Mirror(Level& level, int x, int y);

  void setProperty(std::string property, std::string value) override;
  void setState(int value);

  void update(double dt) override;

};
