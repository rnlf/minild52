#pragma once

#include "entity.hpp"


class b2Body;
class b2Fixture;

class Level;

struct Glass: Entity {
  b2Body *body;
  Sprite sprite;

  void draw() const override;
  
  Glass(Level& level, int x, int y);
};
