#include "level.hpp"
#include "logicgate.hpp"

LogicGate::LogicGate(Level& level, int, int) 
  : Entity(level) {
}

void LogicGate::setProperty(std::string property, std::string value) {
  if(property == "a") {
    setInputA(value == "on");
  } else if(property == "b") {
    setInputB(value == "on");
  } else if(property == "target") {
    target = value;
  } else if(property == "function") {
    setFunction(value);
  }
}


void LogicGate::setInputA(bool v) {
  inA = v;
  updateTargets();
}

void LogicGate::setInputB(bool v) {
  inB = v;
  updateTargets();
}

void LogicGate::setFunction(std::string function) {
  this->function = function;
}


void LogicGate::updateTargets() {
  bool v = false;
  if(function == "and") {
    v = inA && inB;
  } else if(function == "or") {
    v = inA || inB;
  } else if(function == "xor") {
    v = (inA != inB);
  } else if(function == "not") {
    v = !inA;
  }

  sendProperty(target, v ? "on" : "off");
}
