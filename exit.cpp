#include "jojo.hpp"
#include "log.hpp"
#include "exit.hpp"
#include "level.hpp"
#include "loadsprite.hpp"
#include <Box2D/Box2D.h>

#include "statemanager.hpp"
#include "leveldonestate.hpp"
#include "ingame.hpp"
#include "mixer.hpp"
#include "loadsound.hpp"

extern Mixer mixer;

Exit::Exit(Level& level, int x, int y)
  : Entity(level) {

  sprite = loadSprite("data/sprite/exit.spr");

  exitListener.type = Exit::TYPE;
  exitListener.beginContactFunc = [=](b2Fixture* b) { startContact(b); };

  openSound = loadSound("data/sound/dooropen.wav");
  closedSound = loadSound("data/sound/doorclos.wav");

  b2BodyDef bodyDef;
  bodyDef.type = b2_staticBody;
  bodyDef.position.Set(float(x) / 16.0f, float(y) / 16.0f);
  body = level.world.CreateBody(&bodyDef);

  b2PolygonShape box;
  box.SetAsBox(0.15f, 0.75f, b2Vec2(0.5 - 1.0/16.0, 0.5 - 1.0/16.0), 0);
  b2FixtureDef fixtureDef;
  fixtureDef.shape = &box;
  fixtureDef.isSensor = true;
  body->CreateFixture(&fixtureDef);
  body->SetUserData(&exitListener);

  sprite.play("closed");
}

void Exit::update(double dt) {
  sprite.update(dt);
}

void Exit::draw() const {
  auto const& pos = body->GetPosition();
  sprite.draw(pos.x * 16, pos.y * 16);
}

void Exit::startContact(b2Fixture* fixture) {
  auto a = fixture->GetUserData();
  if(a) {
    auto b = reinterpret_cast<EntityContactListener*>(a);
    if(b->type == Jojo::TYPE) {
      if(state && reinterpret_cast<Jojo*>(reinterpret_cast<GenericContactListener*>(b)->data)->state == Jojo::State::ALIVE) {
        auto a = std::make_shared<LevelDoneState>(level.gamestate->manager, "data/level/" + nextlevel + ".tmx", "levldone");
        level.gamestate->manager.switchState(a);
      }
    }
  }
}

void Exit::setProperty(std::string property, std::string value) {
  if(property == "state")
    setState(value == "on");
  if(property == "nextlevel")
    nextlevel = value;
}


void Exit::setState(bool value) {
  if(value == state)
    return;

  float f = 17.0f - sprite.getFrame();
  if(f < 0)
    f = 0.0f;

  log("setting: %d, %d\n", state, value);

  if(value) {
    sprite.play("opening", [=](auto& spr) {
      spr.play("opened"); state = value; 
      mixer.play(openSound);
    }, sprite.getAnimation() == "closing" ? f : 0.0f);
  } else { 
    state = value;
    sprite.play("closing", [=](auto& spr) { spr.play("closed"); }, sprite.getAnimation() == "opening" ? f : 0.0f);
    mixer.play(closedSound);
  }
}
