#pragma once

#include <memory>
#include <string>
#include "surface.hpp"

class Font {
  std::shared_ptr<Surface> bitmap;
  int width;
  int height;

public:
  Font(std::string image);

  void printf(int x, int y, char const* format, ...);
};
