#pragma once

#include <memory>
#include <string>
#include "basesprite.hpp"
#include "sprite.hpp"

std::shared_ptr<BaseSprite> loadBaseSprite(std::string name);

Sprite loadSprite(std::string name, std::string anim="");
