#include <algorithm>
#include "surface.hpp"
#include "draw.hpp"
#include "tools.hpp"

void clear(Surface& dst, uint32_t color) {
  std::fill(dst.getBuffer(),
    dst.getBuffer() + dst.getWidth() * dst.getHeight(),
    color);
}



bool clipSourceAndDestination(
  Surface const& dst, Surface const& src, int &dstx, int &dsty,
  int &srcx, int &srcy, int &srcw, int &srch) noexcept {

  // TODO: Allow flipping
  if(srcw < 0)
    return false;
  if(srch < 0)
    return false;

  if(srcx > int(src.getWidth()))
    return false;
  if(srcy > int(src.getHeight()))
    return false;

  if(srcx < 0) {
    srcw += srcx;
    srcx = 0;
  }

  if(srcy < 0) {
    srch += srcy;
    srcy = 0;
  }

  if(srcw > int(src.getWidth()) - srcx) {
    srcw = int(src.getWidth()) - srcx;
  }

  if(srch > int(src.getHeight()) - srcy) {
    srch = int(src.getHeight()) - srcy;
  }

  
  if(dstx < 0) {
    srcx -= dstx;
    srcw += dstx;
    dstx = 0;
  }

  if(dsty < 0) {
    srcy -= dsty;
    srch += dsty;
    dsty = 0;
  }

  if(srch <= 0 || srcw <= 0)
    return false;

  if(dsty >= int(dst.getHeight())
    || dstx >= int(dst.getWidth()))
    return false;

  if(dsty + srch > int(dst.getHeight()))
    srch = dst.getHeight() - dsty;

  if(dstx + srcw > int(dst.getWidth()))
    srcw = dst.getWidth() - dstx;

  if(srch <= 0 || srcw <= 0)
    return false;

  return true;
}

void blit(Surface const& src, int dstx, int dsty,
                 int srcx, int srcy, int srcw, int srch, 
                 BlitMode mode, bool flipx, bool flipy) {
  blit(*getActiveDrawSurface(), src, dstx, dsty, srcx, srcy, srcw, srch, mode, flipx, flipy);
}

void blit(Surface& dst, Surface const& src, int dstx, int dsty,
                 int srcx, int srcy, int srcw, int srch, 
                 BlitMode mode, bool flipx, bool flipy) {
  switch(mode) {
  case BlitMode::REPLACE:
    blit(dst, src, dstx, dsty, srcx, srcy, srcw, srch, replace, flipx, flipy);
    break;

  case BlitMode::ALPHATEST:
    blit(dst, src, dstx, dsty, srcx, srcy, srcw, srch, alphaTest, flipx, flipy);
    break;

  case BlitMode::ALPHABLEND:
    blit(dst, src, dstx, dsty, srcx, srcy, srcw, srch, alphaBlend, flipx, flipy);
    break;
  };
}
