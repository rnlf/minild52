#include "entity.hpp"
#include "level.hpp"

void Entity::update(double dt) {}
void Entity::draw() const {}
void Entity::setProperty(std::string property, std::string value) {}

void Entity::sendProperty(std::string targets, std::string value) const {
  std::string::size_type i = 0;
  for(;;) {
    std::string::size_type j = targets.find(',', i);
    auto target = targets.substr(i, j-i);
    std::string::size_type cp = target.find(':');
    std::string prop = "state";
    if(cp != std::string::npos) {
      prop = target.substr(cp+1);
      target = target.substr(0,cp);
    }
    auto ent = level.getNamedEntity(target);
    if(ent) {
      ent->setProperty(prop, value);
    }
    if(j == std::string::npos)
      break;
    i = j+1;
  }

}
