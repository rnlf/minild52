#pragma once

#include <string>
#include "gamestate.hpp"
#include "flux.hpp"

class LevelDoneState: public Gamestate {
  std::string nextlevel;
  Surface screenshot;
  std::shared_ptr<Surface> message;
  flux::group tween;
  int tintalpha = 0xFF;

public:
  LevelDoneState(StateManager& manager, std::string nextlevel, std::string message);

  void update(double dt) override;
  void draw() const override;
};
