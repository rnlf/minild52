#pragma once

#include <vector>
#include <map>
#include <string>
#include "surface.hpp"
#include <initializer_list>
#include "draw.hpp"

struct SpriteFrame {
  SpriteFrame(unsigned x, unsigned y, unsigned w, unsigned h);
  unsigned x;
  unsigned y;
  unsigned w;
  unsigned h;
};

struct SpriteAnimation {
  double fps;
  std::vector<SpriteFrame> frames;
  std::string name;
};

class BaseSprite {
  std::shared_ptr<Surface> surface;
  unsigned frameWidth;
  unsigned frameHeight;
  std::map<std::string, SpriteAnimation> animations;
  std::string defaultAnim;
  BlitMode blitMode;
  
public:
  BaseSprite(std::shared_ptr<Surface> surface = nullptr, unsigned frameWidth = 0, unsigned frameHeight = 0, BlitMode mode = BlitMode::REPLACE);
  BaseSprite(BaseSprite const&) = default;
  BaseSprite(BaseSprite&&) = default;
  BaseSprite& operator=(BaseSprite const&) = default;
  BaseSprite& operator=(BaseSprite&&) = default;

  void addAnimation(std::string name, double fps, std::initializer_list<unsigned> frames) {
    addAnimation(name, fps, frames.begin(), frames.end());
  }

  template<typename T>
  void addAnimation(std::string name, double fps, T & frames) {
    addAnimation(name, fps, frames.begin(), frames.end());
  }

  template<typename T>
  void addAnimation(std::string name, double fps, T begin, T end) {
    if(animations.size() == 0)
      defaultAnim = name;

    SpriteAnimation animation;
    animation.name = name;
    animation.fps = fps;
    unsigned framesPerRow = surface->getWidth() / frameWidth;

    for(auto frame = begin; frame != end; ++frame) {
      unsigned col = *frame % framesPerRow;
      unsigned row = *frame / framesPerRow;

      animation.frames.emplace_back(col * frameWidth, row * frameHeight, frameWidth, frameHeight);
    }
    
    animations.emplace(name, animation);
  }

  SpriteAnimation* getAnimation(std::string name);

  Surface const& getSurface() const;

  unsigned getWidth() const;
  unsigned getHeight() const;
  BlitMode getBlitMode() const;
};
