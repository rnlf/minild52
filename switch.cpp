#include "log.hpp"
#include "switch.hpp"
#include "level.hpp"
#include "loadsprite.hpp"

#include <Box2D/Box2D.h>
#include "mixer.hpp"
#include "loadsound.hpp"
extern Mixer mixer;

Switch::Switch(Level& level, int x, int y) 
  : Entity(level, 1) {

  sprite = loadSprite("data/sprite/switch.spr");
  clickSound = loadSound("data/sound/click.wav");

  switchListener.type = TYPE;
  switchListener.beginContactFunc = [=](b2Fixture* f){ this->beginContact(f); };
  switchListener.endContactFunc = [=](b2Fixture* f){ this->endContact(f); };

  b2BodyDef bodyDef;
  bodyDef.type = b2_staticBody;
  bodyDef.position.Set(float(x) / 16.0, float(y) / 16.0);
  body = level.world.CreateBody(&bodyDef);

  b2PolygonShape box;
  box.SetAsBox(5 / 16.0, 2 / 16.0, b2Vec2(6.5/16, 0), 0);
  b2FixtureDef fixtureDef;
  fixtureDef.isSensor = true;
  fixtureDef.shape = &box;
  body->CreateFixture(&fixtureDef)->SetUserData(&switchListener);

  fixtureDef.isSensor = false;
  fixtureDef.friction = 1.5;
  
  b2Vec2 verts[] = {
    {0, 5.5/16.0}, {13/16.0,5.5/16.0}, {11/16.0,2/16.0}, {3/16.0,2/16.0}
  };

  box.Set(verts, 4);
  //box.SetAsBox(7.0 / 16.0, 2 / 16.0, b2Vec2(7.0 / 16.0, 3.5 / 16.0), 0);
  body->CreateFixture(&fixtureDef);
}


void Switch::draw() const {
  auto const& pos = body->GetPosition();
  sprite.draw(pos.x * 16, pos.y * 16);
}

void Switch::setProperty(std::string key, std::string value) {
  if(key == "target")
    target = value;
}

void Switch::beginContact(b2Fixture* f) {
  if(!f->IsSensor()) {
    int oldcont = contacts;
    if(++contacts > 0) {
      if(oldcont == 0) {
        mixer.play(clickSound);
      }
      sprite.play("on");
      sendProperty(target, "on");
    }
  }
}

void Switch::endContact(b2Fixture* f) {
  if(!f->IsSensor())
    if(--contacts == 0) {
      sendProperty(target, "off");
      sprite.play("off");
    }
}
