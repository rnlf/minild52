#pragma once

#include "entity.hpp"
#include "laserblocker.hpp"
#include "sound.hpp"
#include <memory>


class b2Body;
class b2Fixture;

class Level;

struct DynBlock: Entity {
  std::shared_ptr<Sound> thudSound;
  b2Body *body;
  b2Fixture *fixture;
  Sprite sprite;
  LaserBlocker blocker;
  float oldVSpeed = 0.0f;

  void draw() const override;
  void update(double dt) override;
  
  DynBlock(Level& level, int x, int y);
};
