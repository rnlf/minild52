#include "jojoinfluencer.hpp"
#include "jojo.hpp"

#include "log.hpp"
#include "laser.hpp"
#include "math.hpp"

JojoInfluencer::JojoInfluencer(Jojo* jojo, int x, int y)
  : LaserInfluencer(x, y)
  , jojo(jojo) {}

void JojoInfluencer::influence(int & indir, int x, int y) {
  if(jojo->state != Jojo::State::ALIVE)
    return;

  auto const& p = jojo->body->GetPosition();
  auto const& d = Laser::dirs[indir];
  auto dv = pointLineDistance(vec2(p.x + 6.5 / 16.0, p.y + 6.5 / 16.0),
            vec2(float(x) + 8 / 16.0, float(y) + 8.0 / 16.0),
            vec2(float(x) + 8 / 16.0 + d.x, float(y) + 8.0 / 16.0 + d.y));


  if(dv.lensq() < 0.15f) {
    jojo->kill();
  }
}
