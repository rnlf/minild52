#pragma once

#include <string>
#include "entity.hpp"
#include "genericcontactlistener.hpp"
#include "timer.hpp"
#include "sound.hpp"

class b2Body;
class b2Fixture;

class Level;

struct Switch: Entity {
  b2Body *body;
  std::string target;

  std::shared_ptr<Sound> clickSound;
  
  static const int TYPE = 4;

  GenericContactListener switchListener;
  bool state = false;
  Sprite sprite;
  void draw() const override;

  Switch(Level& level, int x, int y);

  void setProperty(std::string property, std::string value) override;

  void beginContact(b2Fixture* f);
  void endContact(b2Fixture* f);

  int contacts = 0;
};
