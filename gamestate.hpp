#pragma once

class StateManager;

class Gamestate {
public:
  StateManager& manager;
  Gamestate(StateManager& manager);
  virtual void update(double dt) = 0;
  virtual void draw() const = 0;


};
